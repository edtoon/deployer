#!/bin/bash

set -o errexit
set -o nounset

SOURCE="${BASH_SOURCE[0]}"
while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
BASE_DIR="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"

. "${BASE_DIR}/lib/config.sh"
. "${BASE_DIR}/lib/os.sh"

if [ ! -f "${EXESUS_JAR}" ];
then
  echo "Building ${EXESUS_JAR}"
  mvn -DskipTests=true package
fi

if [ "cygwin" = "${OPERATING_SYSTEM}" ];
then
  for env_var in "${EXESUS_ENV_PATHS[@]}"
  do
    value="$(eval echo ${!env_var})"
    declare "${env_var}"="$(cygpath -w ${value})"
  done
fi

export EXESUS_DIR

java \
  -Dorg.freemarker.loggerLibrary=slf4j \
  "-Darchaius.configurationSource.additionalUrls=file:///${EXESUS_CONFIG}" \
  -jar "${EXESUS_JAR}"
