#!/bin/bash
# see https://github.com/docker-library/wordpress/blob/master/apache/docker-entrypoint.sh

set -o errexit
set -o nounset

sed_escape_lhs() {
  echo "$@" | sed 's/[]\/$*.^|[]/\\&/g'
}

sed_escape_rhs() {
  echo "$@" | sed 's/[\/&]/\\&/g'
}

php_escape() {
  php -r 'var_export((string) $argv[1]);' "$1"
}

set_config() {
  key="$1"
  value="$2"
  regex="(['\"])$(sed_escape_lhs "$key")\2\s*,"

  if [ "${key:0:1}" = '$' ]; then
    regex="^(\s*)$(sed_escape_lhs "$key")\s*="
  fi

  sed -ri "s/($regex\s*)(['\"]).*\3/\1$(sed_escape_rhs "$(php_escape "$value")")/" /var/www/html/jobs/wp-config.php
}

if [ ! -v WORDPRESS_DB_HOST ];
then
  if [ -v MYSQL_PORT_3306_TCP ];
  then
    WORDPRESS_DB_HOST="mysql"
  else
    echo >&2 "Couldn't find mysql host"
    exit 1
  fi
fi

chown -R www-data:www-data /var/www/html

set_config 'DB_HOST' "$WORDPRESS_DB_HOST"

exec "$@"
