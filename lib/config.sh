#!/bin/bash

if [ ! -v _EXESUS_LIB_DIR_ ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  _EXESUS_LIB_DIR_="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

EXESUS_HOME="${EXESUS_HOME:=${HOME}/.exesus}"
EXESUS_CONFIG="${EXESUS_CONFIG:=${EXESUS_HOME}/config.properties}"
EXESUS_APP="${EXESUS_APP:=$(grep '<artifactId>' "${_EXESUS_LIB_DIR_}/../pom.xml" | head -n 1 | sed 's/artifactId//g' | sed 's/[<>\/ \t]//g')}"
EXESUS_VERSION="${EXESUS_VERSION:=$(grep '<version>' "${_EXESUS_LIB_DIR_}/../pom.xml" | head -n 1 | sed 's/version//g' | sed 's/[<>\/ \t]//g')}"
EXESUS_DIR="${EXESUS_DIR:=$(readlink -f ${_EXESUS_LIB_DIR_}/../)}"
EXESUS_JAR="${EXESUS_JAR:=$(readlink -f ${EXESUS_DIR}/target/${EXESUS_APP}-${EXESUS_VERSION}-with-deps.jar)}"
EXESUS_ENV_PATHS=( \
  "EXESUS_HOME" \
  "EXESUS_CONFIG" \
  "EXESUS_DIR" \
  "EXESUS_JAR" \
)

while IFS= read -r config_line
do
  _exesus_config_property_name_="${config_line%%[ =]*}"
  _exesus_config_property_value_="${config_line#*=}"
  _exesus_config_property_value_="${_exesus_config_property_value_#"${_exesus_config_property_value_%%[![:space:]]*}"}"

  if [ ! -z "${_exesus_config_property_name_}" ] && [ ! -z "${_exesus_config_property_value_}" ]
  then
    declare "${_exesus_config_property_name_}"="${_exesus_config_property_value_}"
  fi

  unset _exesus_config_property_name_
  unset _exesus_config_property_value_
done < "${EXESUS_CONFIG}"
