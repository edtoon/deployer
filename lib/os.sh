#!/bin/bash

if [ ! -v _EXESUS_LIB_DIR_ ];
then
  SOURCE="${BASH_SOURCE[0]}"
  while [ -h "${SOURCE}" ]; do SOURCE="$(readlink "${SOURCE}")"; done
  _EXESUS_LIB_DIR_="$( cd -P "$( dirname "${SOURCE}" )" && pwd )"
fi

. "${_EXESUS_LIB_DIR_}/cmd.sh"

function os-determine
{
  local os="$(uname -o 2>/dev/null || uname -s)"

  if [ "GNU/Linux" = "${os}" ];
  then
    local dist=""

    if cmd-locate lsb_release > /dev/null 2>&1;
    then
      dist="$(lsb_release -si)"
    fi

    if [ -z "${dist}" ] && [ -r /etc/lsb-release ];
    then
      dist="$(. /etc/lsb-release && echo "${DISTRIB_ID}")"
    fi

    if [ -z "${dist}" ] && [ -r /etc/debian_version ];
    then
      dist="debian"
    fi

    if [ -z "${dist}" ] && [ -r /etc/fedora-release ];
    then
      dist="fedora"
    fi

    if [ -z "${dist}" ] && [ -r /etc/os-release ];
    then
      dist="$(. /etc/os-release && echo "${ID}")"
    fi

   if [ ! -z "${dist}" ];
   then
     os="${dist}"
   fi
  fi

  echo "${os}" | tr '[:upper:]' '[:lower:]'
}

export OPERATING_SYSTEM="${OPERATING_SYSTEM:=$(os-determine)}"
