#!/bin/bash

set -o errexit
set -o xtrace

sudo mkdir -p ~${ssh_user}/.ssh
sudo mv -f ${key_path} ~${ssh_user}/.ssh/bitbucket_rsa
sudo chown -R ${ssh_user}:${ssh_user} ~${ssh_user}/.ssh
sudo chown ${ssh_user}:${ssh_user} ~${ssh_user}/.ssh/bitbucket_rsa

SSH_CONF_TMP=`mktemp`
echo -n > ${'$'}{SSH_CONF_TMP}
chmod 0600 ${'$'}{SSH_CONF_TMP}
cat > ${'$'}{SSH_CONF_TMP} << __EOF__
IdentitiesOnly yes
StrictHostKeyChecking no
UserKnownHostsFile /dev/null

Host bitbucket.org
  HostName bitbucket.org
  IdentityFile ~/.ssh/bitbucket_rsa
  IdentitiesOnly yes
  User git
__EOF__
sudo mv -f ${'$'}{SSH_CONF_TMP} ~${ssh_user}/.ssh/config
sudo chown ${ssh_user}:${ssh_user} ~${ssh_user}/.ssh/config
