#!/bin/bash

set -o errexit
set -o xtrace

expect -v > /dev/null 2>&1 || {
  sudo rm -rf /var/lib/apt/lists/*
  sudo apt-get update
  sudo DEBIAN_FRONTEND=noninteractive apt-get -y install expect
}

sudo mkdir -p ${cam_src_dir}/jobs

SRC_TMP_DIR=`mktemp -d`
expect << __EOF__
  set timeout ${rsync_timeout}
  spawn rsync -avzP -e "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -p ${ssh_port} -i ${crosstalk_key_path}" --progress --delete ${crosstalk_user}@${crosstalk_host}:${rsync_crosstalk_dir}/ ${'$'}{SRC_TMP_DIR}/
  expect "Enter passphrase"
  send "${crosstalk_passphrase}\n"
  expect eof
__EOF__
sudo rm -rf ${crosstalk_key_path} ${'$'}{SRC_TMP_DIR}/.git
INDEX_TMP=`mktemp`
cat > ${'$'}{INDEX_TMP} << __EOF__
<?php
/**
 * Front to the WordPress application. This file doesn't do anything, but loads
 * wp-blog-header.php which does and tells WordPress to load the theme.
 *
 * @package WordPress
 */

/**
 * Tells WordPress to load the WordPress theme and output it.
 *
 * @var bool
 */
define('WP_USE_THEMES', true);

/** Loads the WordPress Environment and Template */
require('./jobs/wp-blog-header.php');
?>
__EOF__
chmod 0644 ${'$'}{INDEX_TMP}
sudo chown -R ${ssh_user}:${ssh_user} ${'$'}{SRC_TMP_DIR} ${'$'}{INDEX_TMP}
sudo rm -rf ${cam_src_dir}/jobs ${cam_src_dir}/index.php
sudo mv -f ${'$'}{SRC_TMP_DIR} ${cam_src_dir}/jobs
sudo mv -f ${'$'}{INDEX_TMP} ${cam_src_dir}/index.php
