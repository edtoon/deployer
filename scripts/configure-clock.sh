#!/bin/bash

set -o errexit
set -o xtrace

CLOCK_SERVER="clock.fmt.he.net"

sudo DEBIAN_FRONTEND=noninteractive apt-get install -y openntpd ntpdate
sudo service openntpd stop || true
sudo ntpdate "${CLOCK_SERVER}"
echo "America/Los_Angeles" | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata
sudo service openntpd start
