#!/bin/bash

set -o errexit
set -o xtrace

id ${crosstalk_user} > /dev/null 2>&1 || sudo useradd -m -s /bin/bash ${crosstalk_user}
sudo mkdir -p ~${crosstalk_user}/.ssh
sudo mv -f ${crosstalk_key_path} ~${crosstalk_user}/.ssh/id_rsa.pub
sudo cp -f ~${crosstalk_user}/.ssh/id_rsa.pub ~${crosstalk_user}/.ssh/authorized_keys
sudo chmod -R 0600 ~${crosstalk_user}
sudo chmod 0700 ~${crosstalk_user} ~${crosstalk_user}/.ssh
sudo chown -R ${crosstalk_user}:${crosstalk_user} ~${crosstalk_user}
