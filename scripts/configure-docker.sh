#!/bin/bash

set -o errexit
set -o xtrace

wget -qO- https://get.docker.com/ | sudo sh
