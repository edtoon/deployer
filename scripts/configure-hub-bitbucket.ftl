#!/bin/bash

set -o errexit
set -o xtrace

sudo rm -rf /var/lib/apt/lists/*
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install expect

GIT_TMP_DIR=`mktemp -d`
if [ "" == "${private_key_passphrase}" ];
then
  git clone ${git_args} ${git_repository} ${'$'}{GIT_TMP_DIR}
else
  EXPECT_SCRIPT=`mktemp`
  cat > ${'$'}{EXPECT_SCRIPT} << "  __EOF__"
    set GIT_TMP_DIR [lindex ${'$'}argv 0]
    set timeout ${git_timeout}
    spawn git clone ${git_args} ${git_repository} ${'$'}{GIT_TMP_DIR}
    expect "Enter passphrase"
    send "${private_key_passphrase}\n"
    expect eof
  __EOF__
  expect -f ${'$'}{EXPECT_SCRIPT} -- ${'$'}{GIT_TMP_DIR}
  rm -f ${'$'}{EXPECT_SCRIPT}
fi
sudo mkdir -p ${hub_checkout_dir}
sudo rm -rf ${hub_checkout_dir}
sudo mv -f ${'$'}{GIT_TMP_DIR} ${hub_checkout_dir}
sudo chown -R ${crosstalk_user}:${crosstalk_user} ${hub_checkout_dir}
