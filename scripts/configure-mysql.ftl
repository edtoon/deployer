#!/bin/bash

set -o errexit
set -o xtrace

sudo docker pull mysql:5.7

MYSQL_SERVICE_TMP=`mktemp`
chmod 0600 "${'$'}{MYSQL_SERVICE_TMP}"
cat > "${'$'}{MYSQL_SERVICE_TMP}" << __EOF__
[Unit]
Description=MySQL
After=docker.service
Requires=docker.service

[Service]
ExecStartPre=-/usr/bin/docker kill mysql
ExecStartPre=-/usr/bin/docker rm mysql
ExecStart=/usr/bin/docker run --name mysql -e MYSQL_ROOT_PASSWORD=${root_password} -p 127.0.0.1:3306:3306 -v ${data_dir}:/var/lib/mysql mysql:5.7
Restart=always
RestartSec=1
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
__EOF__
sudo chown root:root "${'$'}{MYSQL_SERVICE_TMP}"
sudo mv -f "${'$'}{MYSQL_SERVICE_TMP}" /etc/systemd/system/mysql.service
unset MYSQL_SERVICE_TMP

sudo systemctl daemon-reload
sudo systemctl enable mysql
sudo systemctl stop mysql
sudo systemctl start mysql
