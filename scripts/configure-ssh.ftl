#!/bin/bash

set -o errexit
set -o xtrace

APT_SOURCES_TMP=`mktemp`
echo -n > "${'$'}{APT_SOURCES_TMP}"
chmod 0600 "${'$'}{APT_SOURCES_TMP}"
cat > "${'$'}{APT_SOURCES_TMP}" << __EOF__
deb http://http.debian.net/debian jessie main
deb-src http://http.debian.net/debian jessie main

deb http://security.debian.org/ jessie/updates main
deb-src http://security.debian.org/ jessie/updates main

deb http://http.debian.net/debian jessie-updates main
deb-src http://http.debian.net/debian jessie-updates main
__EOF__
mv -f "${'$'}{APT_SOURCES_TMP}" /etc/apt/sources.list
unset APT_SOURCES_TMP
apt-get update

DEBIAN_FRONTEND=noninteractive apt-get install -y sudo
id ${ssh_user} > /dev/null 2>&1 || useradd -m -s /bin/bash ${ssh_user}
mkdir -p ~${ssh_user}/.ssh
mv ${key_path} ~${ssh_user}/.ssh/id_rsa.pub
cp -f ~${ssh_user}/.ssh/id_rsa.pub ~${ssh_user}/.ssh/authorized_keys
chmod -R 0600 ~${ssh_user}
chmod 0700 ~${ssh_user} ~${ssh_user}/.ssh
chown -R ${ssh_user}:${ssh_user} ~${ssh_user}
echo '${ssh_user} ALL=(ALL) NOPASSWD: ALL' > /etc/sudoers.d/${ssh_user}
chmod 0440 /etc/sudoers.d/${ssh_user}

SSH_CONF_TMP=`mktemp`
echo -n > "${'$'}{SSH_CONF_TMP}"
chmod 0600 "${'$'}{SSH_CONF_TMP}"
cat > "${'$'}{SSH_CONF_TMP}" << __EOF__
Port ${ssh_port}
Protocol 2
HostKey /etc/ssh/ssh_host_rsa_key
HostKey /etc/ssh/ssh_host_dsa_key
HostKey /etc/ssh/ssh_host_ecdsa_key
HostKey /etc/ssh/ssh_host_ed25519_key
UsePrivilegeSeparation yes
KeyRegenerationInterval 3600
ServerKeyBits 768
SyslogFacility AUTH
LogLevel INFO
LoginGraceTime 30
PermitRootLogin no
StrictModes yes
RSAAuthentication yes
PubkeyAuthentication yes
PasswordAuthentication no
IgnoreRhosts yes
RhostsRSAAuthentication no
HostbasedAuthentication no
PermitEmptyPasswords no
ChallengeResponseAuthentication no
X11Forwarding yes
X11DisplayOffset 10
MaxStartups 3:50:10
PrintMotd no
PrintLastLog yes
TCPKeepAlive yes
AcceptEnv LANG LC_*
Subsystem sftp /usr/lib/openssh/sftp-server
UsePAM yes
UseDNS no
GatewayPorts yes
AllowTcpForwarding yes
ClientAliveInterval 30
ClientAliveCountMax 99999
__EOF__
mv -f "${'$'}{SSH_CONF_TMP}" /etc/ssh/sshd_config
unset SSH_CONF_TMP
service ssh restart

DEBIAN_FRONTEND=noninteractive apt-get install -y fail2ban
FAIL2BAN_JAIL_TMP=`mktemp`
echo -n > "${'$'}{FAIL2BAN_JAIL_TMP}"
chmod 0600 "${'$'}{FAIL2BAN_JAIL_TMP}"
cat > "${'$'}{FAIL2BAN_JAIL_TMP}" << __EOF__
[ssh-iptables]

enabled  = true
filter   = sshd
action   = iptables[name=SSH, port=ssh, protocol=tcp]
logpath  = /var/log/auth.log
maxretry = 5

[ssh-ddos]

enabled = true
port    = ssh,sftp
filter  = sshd-ddos
action   = iptables[name=SSH, port=ssh, protocol=tcp]
logpath  = /var/log/auth.log
maxretry = 2
__EOF__
mv -f "${'$'}{FAIL2BAN_JAIL_TMP}" /etc/fail2ban/jail.local
unset FAIL2BAN_JAIL_TMP
service fail2ban restart
