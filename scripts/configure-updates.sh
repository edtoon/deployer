#!/bin/bash

set -o errexit
set -o xtrace

sudo rm -rf /var/lib/apt/lists/*
sudo apt-get update
sudo dpkg --configure -a
sudo DEBIAN_FRONTEND=noninteractive apt-get -y install -f
sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get -y purge \
  bsd-mailx cloud-utils exim4* mtr-tiny mutt \
  nfs-common rpcbind screen sysstat unattended-upgrades
sudo DEBIAN_FRONTEND=noninteractive apt-get -y autoremove
sudo DEBIAN_FRONTEND=noninteractive apt-get -y upgrade
sudo DEBIAN_FRONTEND=noninteractive apt-get -y autoremove
