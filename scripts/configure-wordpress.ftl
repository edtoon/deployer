#!/bin/bash

set -o errexit
set -o xtrace

echo "CREATE DATABASE IF NOT EXISTS ${wordpress_db_name};" | sudo docker exec -i mysql mysql -uroot -p${mysql_root_password}
echo "GRANT ALL PRIVILEGES ON ${wordpress_db_name}.* TO '${wordpress_db_user}'@'%' IDENTIFIED BY '${wordpress_db_password}';" | sudo docker exec -i mysql mysql -uroot -p${mysql_root_password}

WORDPRESS_BACKUP_GZ="${cam_sql_gz}"
WORDPRESS_BACKUP_SQL="${'$'}{WORDPRESS_BACKUP_GZ:0:(${'$'}{#WORDPRESS_BACKUP_GZ}-3)}"

sudo gunzip "${'$'}{WORDPRESS_BACKUP_GZ}"
sudo docker exec -i mysql mysql -u${wordpress_db_user} -p${wordpress_db_password} ${wordpress_db_name} < "${'$'}{WORDPRESS_BACKUP_SQL}"
sudo gzip "${'$'}{WORDPRESS_BACKUP_SQL}"

WORDPRESS_SERVICE_TMP=`mktemp`
chmod 0600 "${'$'}{WORDPRESS_SERVICE_TMP}"
cat > "${'$'}{WORDPRESS_SERVICE_TMP}" << __EOF__
[Unit]
Description=${container_desc}
After=mysql.service
Requires=docker.service mysql.service

[Service]
ExecStartPre=-/usr/bin/docker kill ${container_name}
ExecStartPre=-/usr/bin/docker rm ${container_name}
ExecStart=/usr/bin/docker run --name ${container_name} --link mysql:mysql -e WORDPRESS_DB_USER=${wordpress_db_user} -e WORDPRESS_DB_PASSWORD=${wordpress_db_password} -e WORDPRESS_DB_NAME=${wordpress_db_name} -v ${src_dir}:/var/www/html -p 80:80 ${docker_image_name}:${docker_image_tag}
Restart=always
RestartSec=1
TimeoutStartSec=0

[Install]
WantedBy=multi-user.target
__EOF__
sudo chown root:root "${'$'}{WORDPRESS_SERVICE_TMP}"
sudo mv -f "${'$'}{WORDPRESS_SERVICE_TMP}" /etc/systemd/system/${container_name}.service
unset WORDPRESS_SERVICE_TMP

sudo systemctl daemon-reload
sudo systemctl enable ${container_name}
sudo systemctl stop ${container_name}
sudo systemctl start ${container_name}
