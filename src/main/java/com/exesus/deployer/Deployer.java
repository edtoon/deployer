package com.exesus.deployer;

import com.exesus.deployer.command.*;
import com.exesus.deployer.config.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.jcraft.jsch.Session;
import com.myjeeva.digitalocean.DigitalOcean;
import com.myjeeva.digitalocean.common.DropletStatus;
import com.myjeeva.digitalocean.impl.DigitalOceanClient;
import com.myjeeva.digitalocean.pojo.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.solong.linode4j.*;
import uk.co.solong.linode4j.jobmanager.JobManager;
import uk.co.solong.linode4j.mappings.*;

import java.io.File;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Deployer {
    private static final Logger log = LoggerFactory.getLogger(Deployer.class);

    private static final int DEFAULT_SSH_PORT = 22;
    private static final DigitalOcean digitalOceanClient = new DigitalOceanClient(Config.getString(Config.Key.DIGITAL_OCEAN_TOKEN));
    private static final Linode linodeApi = new Linode(Config.getString(Config.Key.LINODE_API_KEY));
    private static final Map<Integer, HostConfiguration> hostConfigByPriorityMap = new TreeMap<>();
    private static final Set<Provider> initializedProviders = new HashSet<>();

    static {
        try {
            hostConfigByPriorityMap.put(1, LinodeHostConfig.createConfiguration(LinodeHostConfig.LINODE_HOST_FREMONT_HUB));
            hostConfigByPriorityMap.put(2, DigitalOceanHostConfig.createConfiguration(DigitalOceanHostConfig.DIGITAL_OCEAN_HOST_CAM_DEV));
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private interface ConfiguredDropletAction {
        void execute(HostConfiguration hostConfiguration, Droplet droplet) throws Exception;
    }

    private interface ConfiguredLinodeAction {
        void execute(HostConfiguration hostConfiguration, LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration) throws Exception;
    }

    public static void main(String[] args) {
        try {
            Set<HostConfiguration> provisionedHosts = new LinkedHashSet<>();

            for(HostConfiguration hostConfiguration : hostConfigByPriorityMap.values()) {
                String hostName = hostConfiguration.getHostName();
                Provider provider = (
                    hostConfiguration.getDroplet() != null ?
                        Provider.DIGITAL_OCEAN :
                        (hostConfiguration.getLinodeHostConfiguration() != null ? Provider.LINODE : null)
                );

                if(provider == null) {
                    log.error("Host: " + hostName + " does not seem to have a provider associated with it");
                    continue;
                }

                initializeProvider(hostConfiguration, provider, provisionedHosts);

                String ipv4Address = getRunningHostIpv4(provider, hostConfiguration);

                if(StringUtils.isBlank(ipv4Address)) {
                    log.error("Host: " + hostName + " does not seem to have an IPv4 address");
                    continue;
                }

                hostConfiguration.setIpv4Address(ipv4Address);

                if(provisionHostAccess(hostConfiguration)) {
                    provisionHost(hostConfiguration);

                    provisionedHosts.add(hostConfiguration);
                }
            }
        } catch(Exception e) {
            log.error("Error deploying hosts", e);
            System.exit(1);
        }
    }

    private static String getRunningHostIpv4(Provider provider, HostConfiguration hostConfiguration) throws Exception {
        String hostName = hostConfiguration.getHostName();

        switch(provider) {
            case DIGITAL_OCEAN:
                Droplet droplet = hostConfiguration.getDroplet();
                Integer dropletId = droplet.getId();
                DropletStatus dropletStatus = droplet.getStatus();

                while(DropletStatus.NEW == dropletStatus) {
                    log.debug("Waiting for dropletId: " + dropletId + ", host: " + hostName + " to become active");

                    Utils.sleepQuietly(Config.getLong(Config.Key.DEPLOY_AWAIT_DROPLET_ACTIVE_INTERVAL));

                    droplet = digitalOceanClient.getDropletInfo(dropletId);
                    dropletStatus = (droplet == null ? null : droplet.getStatus());

                    hostConfiguration.setDroplet(droplet);
                }

                if(DropletStatus.ACTIVE == dropletStatus) {
                    return Utils.getDropletDefaultIpv4(droplet);
                }

                log.warn("Host: " + hostName + ", dropletId: " + dropletId + " is in state: " + dropletStatus);

                break;
            case LINODE:
                LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration = hostConfiguration.getLinodeHostConfiguration();
                Integer status = linodeHostConfiguration.getStatus();
                LinodeHostConfig.LinodeHostStatus statusEnum = (status == null ? null : LinodeHostConfig.LinodeHostStatus.valueOfQuietly(status));

                if(LinodeHostConfig.LinodeHostStatus.RUNNING == statusEnum) {
                    return Utils.getLinodeDefaultIpv4(linodeApi, linodeHostConfiguration);
                }

                log.warn("Host: " + hostName + ", linodeId: " + linodeHostConfiguration.getLinodeId() + " is in state: " + statusEnum);

                break;
        }

        return null;
    }

    private static boolean provisionHostAccess(HostConfiguration hostConfiguration) throws Exception {
        Droplet droplet = hostConfiguration.getDroplet();
        String hostName = hostConfiguration.getHostName();
        String ipv4Address = hostConfiguration.getIpv4Address();
        Integer reachableSshPort = new ReachablePortCommand(
            ipv4Address, new int[]{DEFAULT_SSH_PORT, Config.getInt(Config.Key.SSH_PORT)},
            Config.getInt(Config.Key.DEPLOY_CHECK_SSH_PORT_RETRY_LIMIT),
            Config.getLong(Config.Key.DEPLOY_CHECK_SSH_PORT_RETRY_INTERVAL)
        ).execute();

        if(reachableSshPort == null) {
            log.error("Host: " + hostName + ", ipv4: " + ipv4Address + " is not reachable via ssh");
            return false;
        }

        File privateKeyFile = Config.getFile((droplet != null ? Config.Key.DIGITAL_OCEAN_PVT_KEY : Config.Key.LINODE_PVT_KEY));
        boolean defaultSshPortWasReachable = (DEFAULT_SSH_PORT == reachableSshPort);

        if(defaultSshPortWasReachable) {
            log.debug("Host: " + hostName + ", ipv4: " + ipv4Address + " is reachable on port " + DEFAULT_SSH_PORT);

            File publicKeyFile = Config.getFile((droplet != null ? Config.Key.DIGITAL_OCEAN_PUB_KEY : Config.Key.LINODE_PUB_KEY));
            Boolean accessResult = new ConfigureSshCommand(ipv4Address, DEFAULT_SSH_PORT, "root", privateKeyFile, publicKeyFile).execute();

            if(!Boolean.TRUE.equals(accessResult)) {
                throw new Exception("Host: " + hostName + ", ipv4: " + ipv4Address + " could not configure ssh access");
            }

            Utils.sleepQuietly(Config.getLong(Config.Key.DEPLOY_WAIT_AFTER_SSH_CONFIG));
        }

        return true;
    }

    private static void provisionHost(HostConfiguration hostConfiguration) throws Exception {
        String ipv4Address = hostConfiguration.getIpv4Address();
        File privateKeyFile = Config.getFile((hostConfiguration.getDroplet() != null ? Config.Key.DIGITAL_OCEAN_PVT_KEY : Config.Key.LINODE_PVT_KEY));
        Session session = SshUtil.createSession(privateKeyFile, null, Config.getString(Config.Key.SSH_USER), ipv4Address, Config.getInt(Config.Key.SSH_PORT));

        try {
            String hostName = hostConfiguration.getHostName();

            log.debug("Provisioning host: " + hostName + ", ipv4: " + ipv4Address);

            provisionScriptWithLock(session, hostName, ProvisionScript.UPDATES, null, null);
            provisionScriptWithLock(session, hostName, ProvisionScript.CLOCK, null, null);
            provisionScriptWithLock(session, hostName, ProvisionScript.DOCKER, null, null);

            for(String remotePath : hostConfiguration.getDirectories()) {
                Pair<String, String> ownerAndMode = hostConfiguration.getDirectoryOwnerAndMode(remotePath);
                String owner = ownerAndMode.getKey();

                SshUtil.execute(session, log, "id " + owner + " > /dev/null 2>&1 || sudo useradd -m -s /bin/bash " + owner);
                SshUtil.execute(
                    session, log, "sudo -- sh -c \"" +
                        "mkdir -p " + remotePath + " && " +
                        "chown " + owner + ":" + owner + " " + remotePath + " && " +
                        "chmod " + ownerAndMode.getValue() + " " + remotePath +
                        "\""
                );
            }

            for(Pair<ProvisionFile, String> provisionFileAtPath : hostConfiguration.getProvisionFilesAtPaths()) {
                ProvisionFile provisionFile = provisionFileAtPath.getKey();

                if(!new ProvisionFileCommand(provisionFile, provisionFileAtPath.getValue(), session).execute()) {
                    throw new Exception("Host: " + hostName + ", ipv4: " + ipv4Address + " could not provision file: " + provisionFile.name());
                }
            }

            for(ProvisionImage provisionImage : hostConfiguration.getProvisionImages()) {
                if(!new ProvisionImageCommand(provisionImage, session).execute()) {
                    throw new Exception("Host: " + hostName + ", ipv4: " + ipv4Address + " could not provision image: " + provisionImage.name());
                }
            }

            for(ProvisionScript provisionScript : hostConfiguration.getProvisionScripts()) {
                Object templateData = hostConfiguration.getProvisionScriptTemplateData(provisionScript);
                Set<Pair<File, String>> filesAtPaths = hostConfiguration.getProvisionFilesForScript(provisionScript);

                provisionScriptWithLock(session, hostName, provisionScript, templateData, filesAtPaths);
            }
        } finally {
            session.disconnect();
        }
    }

    private static void provisionScriptWithLock(Session session, String hostName, ProvisionScript provisionScript, Object templateData, Set<Pair<File, String>> filesAtPaths) throws Exception {
        String provisionScriptName = provisionScript.name();
        String hostProvisionDir = Config.getString(Config.Key.HOST_PROVISION_DIR);
        String provisionLockPath = hostProvisionDir + "/" + provisionScriptName.toLowerCase();

        SshUtil.SshCommandResult duResult = SshUtil.executeWithResult(session, "sudo du -b " + provisionLockPath);

        if(duResult.getExitStatus() != 0) {
            if(filesAtPaths == null || filesAtPaths.isEmpty()) {
                if(!new ProvisionScriptCommand(provisionScript, session, templateData).execute()) {
                    throw new Exception("Host: " + hostName + " could not provision script: " + provisionScriptName);
                }
            } else if(!new ProvisionFilesAndScriptCommand(filesAtPaths, provisionScript, session, templateData).execute()) {
                throw new Exception("Host: " + hostName + " could not provision script: " + provisionScriptName + " or its associated files");
            }

            SshUtil.execute(session, log, "sudo mkdir -p " + hostProvisionDir);
            SshUtil.execute(session, "date +%s | sudo tee " + provisionLockPath);
        }
    }

    private static void initializeProvider(HostConfiguration hostConfiguration, Provider provider, Set<HostConfiguration> provisionedHosts) throws Exception {
        switch(provider) {
            case DIGITAL_OCEAN:
                if(!initializedProviders.contains(provider)) {
                    evaluateDigitalOceanRegions();
                    evaluateDigitalOceanImages();
                    evaluateAvailableDroplets();

                    int deployedDropletCount = deployDroplets();

                    if(deployedDropletCount > 0) {
                        log.debug("Deployed " + deployedDropletCount + " droplets");
                    }

                    initializedProviders.add(provider);
                }

                DigitalOceanHostConfig.configureProvisioners(hostConfiguration, provisionedHosts);

                break;
            case LINODE:
                if(!initializedProviders.contains(provider)) {
                    evaluateLinodeDataCenters();
                    evaluateLinodePlans();
                    evaluateLinodeDistributions();
                    evaluateLinodeKernels();
                    evaluateAvailableLinodes();

                    int deployedLinodeCount = deployLinodes();

                    if(deployedLinodeCount > 0) {
                        log.debug("Deployed " + deployedLinodeCount + " linodes");
                    }

                    initializedProviders.add(provider);
                }

                LinodeHostConfig.configureProvisioners(hostConfiguration, provisionedHosts);

                break;
        }
    }

    private static int deployLinodes() throws Exception {
        final AtomicInteger deployedCounter = new AtomicInteger(0);

        forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
            Integer dataCenterId = linodeHostConfiguration.getDataCenterId();
            Integer planId = linodeHostConfiguration.getPlanId();

            if(dataCenterId == null || planId == null) {
                return;
            }

            String linodeLabel = linodeHostConfiguration.getLabel();
            Integer linodeId = linodeHostConfiguration.getLinodeId();

            if(linodeId == null) {
                JsonNode createdLinodeNode = linodeApi.createLinode(dataCenterId, planId).asJson();
                JsonNode dataNode = (createdLinodeNode == null ? null : createdLinodeNode.get("DATA"));
                JsonNode linodeIdNode = (dataNode == null ? null : dataNode.get("LinodeID"));

                linodeId = (linodeIdNode == null ? null : linodeIdNode.asInt());

                if(linodeId == null || linodeId <= 0) {
                    throw new Exception("Tried to create linode: " + linodeLabel + " but couldn't get linodeId from createLinode result: " + (createdLinodeNode == null ? null : createdLinodeNode.toString()));
                }

                log.info("Linode: " + linodeLabel + " created, linodeId: " + linodeId);

                linodeHostConfiguration.setLinodeId(linodeId);

                deployedCounter.incrementAndGet();
            }

            String displayGroup = linodeHostConfiguration.getDisplayGroup();
            UpdateLinodeBuilder updateLinodeBuilder = linodeApi.updateLinode(linodeId)
                .withLabel(linodeLabel)
                .withAlertCpuThreshold(90)
                .withAlertDiskIoThreshold(1000)
                .withAlertBwInThreshold(5)
                .withAlertBwOutThreshold(5)
                .withAlertBwQuotaThreshold(80)
                .withWatchdog(true);

            if(!StringUtils.isBlank(displayGroup)) {
                updateLinodeBuilder.withLpmDisplayGroup(displayGroup);
            }

            JsonNode updateLinodeResultNode = updateLinodeBuilder.asJson();
            JsonNode updateLinodeErrorArrayNode = (updateLinodeResultNode == null ? null : updateLinodeResultNode.get("ERRORARRAY"));

            if(updateLinodeErrorArrayNode != null && updateLinodeErrorArrayNode.size() > 0) {
                throw new Exception("Linode: " + linodeLabel + ", linodeId: " + linodeId + " could not be updated, result was: " + updateLinodeResultNode.toString());
            }

            waitForLinodePendingJobs(linodeHostConfiguration);

            Set<LinodeHostConfig.LinodeHostDisk> disks = linodeHostConfiguration.getDisks();
            Integer totalHd = linodeHostConfiguration.getTotalHd();
            int remainingDiskSpace = totalHd;
            LinodeHostConfig.LinodeHostDisk unspecifiedSizedDisk = null;
            int unspecifiedSizeDiskCount = 0;

            for(LinodeHostConfig.LinodeHostDisk linodeHostDisk : disks) {
                Integer size = linodeHostDisk.getSize();

                if(size == null) {
                    unspecifiedSizeDiskCount++;

                    if(unspecifiedSizedDisk == null) {
                        unspecifiedSizedDisk = linodeHostDisk;
                    }
                } else {
                    remainingDiskSpace -= size;
                }
            }

            if(unspecifiedSizeDiskCount > 1) {
                throw new Exception("Linode: " + linodeLabel + ", linodeId: " + linodeId + " has " + unspecifiedSizeDiskCount + " disks of unspecified size configured, total size is: " + totalHd);
            } else if(unspecifiedSizedDisk != null) {
                unspecifiedSizedDisk.setSize(remainingDiskSpace);
            }

            for(LinodeHostConfig.LinodeHostDisk linodeHostDisk : disks) {
                if(linodeHostDisk.getDiskId() == null) {
                    createLinodeHostDisk(linodeHostConfiguration, linodeHostDisk);
                }
            }

            String configLabel = linodeHostConfiguration.getConfigLabel();
            Integer kernelId = linodeHostConfiguration.getKernelId();

            if(!StringUtils.isBlank(configLabel) && kernelId != null && linodeHostConfiguration.getConfigId() == null) {
                String diskList = disks.stream().map(disk -> disk.getDiskId().toString()).collect(Collectors.joining(","));
                JsonNode createdLinodeConfigNode = linodeApi.createLinodeConfig(linodeId, kernelId, configLabel, diskList)
                    .withHelperNetwork(true)
                    .asJson();
                JsonNode createdConfigErrorArrayNode = (createdLinodeConfigNode == null ? null : createdLinodeConfigNode.get("ERRORARRAY"));

                if(createdConfigErrorArrayNode != null && createdConfigErrorArrayNode.size() > 0) {
                    throw new Exception("Config: " + configLabel + " could not be created for linode: " + linodeLabel + ", result was: " + createdConfigErrorArrayNode.toString());
                }

                JsonNode createdConfigDataNode = (createdLinodeConfigNode == null ? null : createdLinodeConfigNode.get("DATA"));

                if(createdConfigDataNode == null) {
                    throw new Exception("Attempted to create config: " + configLabel + " for linode: " + linodeLabel + " but did not get disk data, result was: " + (createdLinodeConfigNode == null ? null : createdLinodeConfigNode.toString()));
                }

                int createdConfigId = createdConfigDataNode.get("ConfigID").asInt();

                linodeHostConfiguration.setConfigId(createdConfigId);
            }

            if(LinodeHostConfig.LinodeHostStatus.RUNNING != LinodeHostConfig.LinodeHostStatus.valueOfQuietly(linodeHostConfiguration.getStatus())) {
                Integer configId = linodeHostConfiguration.getConfigId();
                RebootLinodeBuilder rebootLinodeBuilder = linodeApi.rebootLinode(linodeId);

                if(configId != null) {
                    rebootLinodeBuilder.withConfigId(configId);
                }

                JsonNode rebootResultNode = rebootLinodeBuilder.asJson();
                JsonNode rebootResultDataNode = (rebootResultNode == null ? null : rebootResultNode.get("DATA"));
                Integer jobId = (rebootResultDataNode == null ? null : rebootResultDataNode.get("JobID").asInt());

                if(jobId == null || jobId <= 0) {
                    throw new Exception("Rebooted linode: " + linodeLabel + " with configId: " + configId + " but didn't get a jobId, result: " + (rebootResultNode == null ? null : rebootResultNode.toString()));
                }

                log.info("Rebooted linode: " + linodeLabel + " with configId: " + configId + ", waiting for jobId: " + jobId + " to finish");

                new JobManager(linodeApi).waitForJob(linodeId, jobId);
            }
        });

        return deployedCounter.get();
    }

    private static void waitForLinodePendingJobs(LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration) {
        while(true) {
            JsonNode pendingJobsNode = linodeApi.listJobs(linodeHostConfiguration.getLinodeId()).withPendingOnly(true).asJson();
            JsonNode pendingJobsDataNode = (pendingJobsNode == null ? null : pendingJobsNode.get("DATA"));
            int unfinishedJobsCount = 0;

            if(pendingJobsDataNode != null) {
                for(JsonNode pendingJobNode : pendingJobsDataNode) {
                    int hostSuccess = pendingJobNode.get("HOST_SUCCESS").intValue();

                    if(hostSuccess != 1) {
                        unfinishedJobsCount++;
                    }
                }
            }

            if(unfinishedJobsCount <= 0) {
                break;
            }

            log.debug("Waiting for " + unfinishedJobsCount + " on linode: " + linodeHostConfiguration.getLabel() + " to finish, pending jobs: " + pendingJobsDataNode.toString());

            Utils.sleepQuietly(Config.getLong(Config.Key.DEPLOY_LINODE_WAIT_PENDING_JOBS));
        }
    }

    private static void createLinodeHostDisk(LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration, LinodeHostConfig.LinodeHostDisk linodeHostDisk) throws Exception {
        String linodeLabel = linodeHostConfiguration.getLabel();
        Integer linodeId = linodeHostConfiguration.getLinodeId();
        Integer distributionId = linodeHostConfiguration.getDistributionId();
        String diskLabel = linodeHostDisk.getLabel();
        String diskType = linodeHostDisk.getType();
        Integer diskSize = linodeHostDisk.getSize();
        File publicKeyFile = Config.getFile(Config.Key.LINODE_PUB_KEY);
        String publicKey = (publicKeyFile == null ? null : FileUtils.readFileToString(publicKeyFile));
        String password = UUID.randomUUID().toString();

        JsonNode createdDiskNode;
        boolean diskTypeIsBlank = StringUtils.isBlank(diskType);

        if(!diskTypeIsBlank || !Boolean.TRUE.equals(linodeHostDisk.getIsFromDistribution())) {
            if(diskTypeIsBlank) {
                diskType = LinodeHostConfig.LINODE_DISK_TYPE_DEFAULT;
            }

            CreateLinodeDiskBuilder createLinodeDiskBuilder = linodeApi.createLinodeDisk(linodeId, diskLabel, diskType, diskSize);

            if(!LinodeHostConfig.LINODE_DISK_TYPE_SWAP.equals(diskType)) {
                createLinodeDiskBuilder.withRootPass(password);

                if(!StringUtils.isBlank(publicKey)) {
                    createLinodeDiskBuilder.withRootSSHKey(publicKey);
                }
            }

            createdDiskNode = createLinodeDiskBuilder.asJson();
        } else {
            CreateLinodeDiskFromDistributionBuilder createDiskFromDistributionBuilder = linodeApi.createLinodeDiskFromDistribution(
                linodeId, distributionId, diskLabel, diskSize, password
            );

            if(!StringUtils.isBlank(publicKey)) {
                createDiskFromDistributionBuilder.withRootSSHKey(publicKey);
            }

            createdDiskNode = createDiskFromDistributionBuilder.asJson();
        }

        JsonNode createdDiskErrorArrayNode = (createdDiskNode == null ? null : createdDiskNode.get("ERRORARRAY"));

        if(createdDiskErrorArrayNode != null && createdDiskErrorArrayNode.size() > 0) {
            throw new Exception("Disk: " + diskLabel + " could not be created for linode: " + linodeLabel + ", result was: " + createdDiskErrorArrayNode.toString());
        }

        JsonNode createdDiskDataNode = (createdDiskNode == null ? null : createdDiskNode.get("DATA"));

        if(createdDiskDataNode == null) {
            throw new Exception("Attempted to create disk: " + diskLabel + " for linode: " + linodeLabel + " but did not get disk data, result was: " + (createdDiskNode == null ? null : createdDiskNode.toString()));
        }

        int diskId = createdDiskDataNode.get("DiskID").asInt();
        int jobId = createdDiskDataNode.get("JobID").asInt();

        linodeHostDisk.setDiskId(diskId);

        log.info("Created disk: " + diskLabel + ", diskId: " + diskId + " for linode: " + linodeLabel + ", waiting for jobId: " + jobId + " to finish");

        new JobManager(linodeApi).waitForJob(linodeId, jobId);
    }

    private static int deployDroplets() throws Exception {
        AtomicInteger deployedCounter = new AtomicInteger(0);

        forEachConfiguredDroplet((hostConfiguration, droplet) -> {
            if(droplet.getStatus() != null) {
                return;
            }

            log.info("Trying to deploy in region: " + droplet.getRegion().getSlug() + ", image id: " + droplet.getImage().getId() + ", image name: " + droplet.getImage().getSlug());

            Droplet deployedDroplet = digitalOceanClient.createDroplet(droplet);

            log.info("Deployed droplet id: " + deployedDroplet.getId() + ", name: " + deployedDroplet.getName());

            hostConfiguration.setDroplet(deployedDroplet);
            deployedCounter.incrementAndGet();
        });

        return deployedCounter.get();
    }

    private static void evaluateAvailableLinodes() throws Exception {
        JsonNode linodeListNode = linodeApi.listLinode().asJson();
        JsonNode linodeDataNode = (linodeListNode == null ? null : linodeListNode.get("DATA"));

        if(linodeDataNode == null || linodeDataNode.size() <= 0) {
            log.debug("No linodes available" + (linodeListNode == null ? "" : linodeListNode.toString()));
            return;
        }

        for(JsonNode linodeNode : linodeDataNode) {
            String linodeLabel = linodeNode.get("LABEL").asText();
            int linodeId = linodeNode.get("LINODEID").asInt();
            int linodeDataCenterId = linodeNode.get("DATACENTERID").asInt();
            int linodePlanId = linodeNode.get("PLANID").asInt();
            int linodeStatus = linodeNode.get("STATUS").asInt();
            int linodeTotalHd = linodeNode.get("TOTALHD").asInt();

            forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
                String configuredLinodeLabel = linodeHostConfiguration.getLabel();

                if(!linodeLabel.equals(configuredLinodeLabel)) {
                    return;
                }

                linodeHostConfiguration.setStatus(linodeStatus);

                Integer configuredLinodeId = linodeHostConfiguration.getLinodeId();
                Integer configuredDataCenterId = linodeHostConfiguration.getDataCenterId();
                Integer configuredPlanId = linodeHostConfiguration.getPlanId();
                Integer configuredTotalHd = linodeHostConfiguration.getTotalHd();

                if(configuredLinodeId == null) {
                    linodeHostConfiguration.setLinodeId(linodeId);
                } else if(!configuredLinodeId.equals(linodeId)) {
                    throw new Exception("Linode: " + linodeLabel + " has linodeId: " + linodeId + " but was configured with linodeId: " + configuredLinodeId);
                }

                if(configuredDataCenterId == null) {
                    linodeHostConfiguration.setDataCenterId(linodeDataCenterId);
                } else if(!configuredDataCenterId.equals(linodeDataCenterId)) {
                    throw new Exception("Linode: " + linodeLabel + " has dataCenterId: " + linodeDataCenterId + " but was configured with dataCenterId: " + configuredDataCenterId);
                }

                if(configuredPlanId == null) {
                    linodeHostConfiguration.setPlanId(linodePlanId);
                } else if(!configuredPlanId.equals(linodePlanId)) {
                    throw new Exception("Linode: " + linodeLabel + " has planId: " + linodePlanId + " but was configured with planId: " + configuredPlanId);
                }

                if(configuredTotalHd == null) {
                    linodeHostConfiguration.setTotalHd(linodeTotalHd);
                } else if(!configuredTotalHd.equals(linodeTotalHd)) {
                    throw new Exception("Linode: " + linodeLabel + " has totalHd: " + linodeTotalHd + " but was configured with totalHd: " + configuredTotalHd);
                }

                JsonNode disksNode = linodeApi.listLinodeDisk(linodeId).asJson();
                JsonNode disksDataNode = (disksNode == null ? null : disksNode.get("DATA"));

                if(disksDataNode == null) {
                    return;
                }

                evaluateAvailableLinodeDisks(linodeHostConfiguration, disksDataNode);

                String configLabel = linodeHostConfiguration.getConfigLabel();

                if(StringUtils.isBlank(configLabel)) {
                    return;
                }

                JsonNode configNode = linodeApi.listLinodeConfigs(linodeId).asJson();
                LinodeConfigMapper linodeConfigMapper = new LinodeConfigMapper(configNode);
                int configId;

                try {
                    configId = linodeConfigMapper.getConfigIdFromLinodeId(configLabel);
                } catch(Exception e) {
                    return;
                }

                linodeHostConfiguration.setConfigId(configId);
            });
        }
    }

    private static void evaluateAvailableLinodeDisks(LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration, JsonNode disksDataNode) throws Exception {
        String linodeLabel = linodeHostConfiguration.getLabel();

        for(JsonNode diskNode : disksDataNode) {
            String diskLabel = diskNode.get("LABEL").asText();

            if(StringUtils.isBlank(diskLabel)) {
                log.warn("Found linode disk with no label: " + disksDataNode.toString());
                continue;
            }

            Integer diskId = diskNode.get("DISKID").asInt();
            String diskType = diskNode.get("TYPE").asText();
            Boolean diskIsReadOnly = (diskNode.get("ISREADONLY").asInt() == 1);
            Integer diskStatus = diskNode.get("STATUS").asInt();
            Integer diskSize = diskNode.get("SIZE").asInt();

            boolean diskIsConfigured = false;

            for(LinodeHostConfig.LinodeHostDisk configuredDisk : linodeHostConfiguration.getDisks()) {
                if(!diskLabel.equals(configuredDisk.getLabel())) {
                    continue;
                }

                diskIsConfigured = true;

                Integer configuredDiskId = configuredDisk.getDiskId();
                String configuredDiskType = configuredDisk.getType();
                Boolean configuredDiskIsReadOnly = configuredDisk.getIsReadOnly();
                Integer configuredDiskStatus = configuredDisk.getStatus();
                Integer configuredDiskSize = configuredDisk.getSize();

                if(configuredDiskId == null) {
                    configuredDisk.setDiskId(diskId);
                } else if(!configuredDiskId.equals(diskId)) {
                    throw new Exception("Disk: " + diskLabel + " on linode: " + linodeLabel + " has id: " + diskId + " but was configured with id: " + configuredDiskId);
                }

                if(configuredDiskType == null) {
                    configuredDisk.setType(diskType);
                } else if(!configuredDiskType.equals(diskType)) {
                    throw new Exception("Disk: " + diskLabel + " on linode: " + linodeLabel + " has type: " + diskType + " but was configured with type: " + configuredDiskType);
                }

                if(configuredDiskIsReadOnly == null) {
                    configuredDisk.setIsReadOnly(diskIsReadOnly);
                } else if(!configuredDiskIsReadOnly.equals(diskIsReadOnly)) {
                    throw new Exception("Disk: " + diskLabel + " on linode: " + linodeLabel + " has readonly: " + diskIsReadOnly + " but was configured with readonly: " + configuredDiskIsReadOnly);
                }

                if(configuredDiskStatus == null) {
                    configuredDisk.setStatus(diskStatus);
                } else if(!configuredDiskStatus.equals(diskStatus)) {
                    throw new Exception("Disk: " + diskLabel + " on linode: " + linodeLabel + " has status: " + diskStatus + " but was configured with status: " + configuredDiskStatus);
                }

                if(configuredDiskSize == null) {
                    configuredDisk.setSize(diskSize);
                } else if(!configuredDiskSize.equals(diskSize)) {
                    throw new Exception("Disk: " + diskLabel + " on linode: " + linodeLabel + " has size: " + diskSize + " but was configured with size: " + configuredDiskSize);
                }
            }

            if(!diskIsConfigured) {
                log.warn("Disk: " + diskLabel + " on linode: " + linodeLabel + " exists but is not in configuration");

                LinodeHostConfig.LinodeHostDisk unrecognizedDisk = new LinodeHostConfig.LinodeHostDisk();

                unrecognizedDisk.setLabel(diskLabel);
                unrecognizedDisk.setDiskId(diskId);
                unrecognizedDisk.setType(diskType);
                unrecognizedDisk.setIsReadOnly(diskIsReadOnly);
                unrecognizedDisk.setStatus(diskStatus);
                unrecognizedDisk.setSize(diskSize);

                linodeHostConfiguration.addDisk(unrecognizedDisk);
            }
        }
    }

    private static void evaluateAvailableDroplets() throws Exception {
        int total = 0;
        AtomicInteger deployedCounter = new AtomicInteger(0);
        AtomicInteger unrecognizedCounter = new AtomicInteger(0);
        int pageNumber = 0;

        do {
            ++pageNumber;

            Droplets droplets = digitalOceanClient.getAvailableDroplets(pageNumber);

            if(droplets == null || droplets.getDroplets() == null || droplets.getDroplets().isEmpty()) {
                log.debug("No droplets at page: " + pageNumber);
                break;
            }

            logDigitalOceanRequestInfo("droplets", droplets, pageNumber);

            total = droplets.getMeta().getTotal();

            for(Droplet runningDroplet : droplets.getDroplets()) {
                Integer dropletId = runningDroplet.getId();
                String dropletName = runningDroplet.getName();
                String ipv4Address = Utils.getDropletDefaultIpv4(runningDroplet);
                final AtomicBoolean dropletRecognized = new AtomicBoolean(false);

                forEachConfiguredDroplet((hostConfiguration, configuredDroplet) -> {
                    if(!dropletName.equals(configuredDroplet.getName())) {
                        return;
                    }

                    log.debug("Droplet id: " + dropletId + ", name: " + dropletName + ", ipv4: " + ipv4Address + " was recognized");
                    dropletRecognized.set(true);
                    hostConfiguration.setDroplet(runningDroplet);
                    deployedCounter.incrementAndGet();
                });

                if(!dropletRecognized.get()) {
                    log.warn("Droplet id: " + dropletId + ", name: " + dropletName + ", ipv4: " + ipv4Address + " was not recognized");
                    unrecognizedCounter.incrementAndGet();
                }
            }
        } while((deployedCounter.get() + unrecognizedCounter.get()) < total);

        log.debug("Total droplets: " + total + ", deployed: " + deployedCounter.get() + ", unrecognized: " + unrecognizedCounter.get());
    }

    private static void evaluateDigitalOceanImages() throws Exception {
        Map<String, Image> availableImagesBySlug = new HashMap<>();
        Map<Integer, Image> availableImagesById = new HashMap<>();

        forEachConfiguredDroplet((hostConfiguration, droplet) -> {
            String dropletName = droplet.getName();
            Image dropletImage = droplet.getImage();

            if(dropletImage == null) {
                throw new Exception("Host: " + dropletName + " has no image specified");
            }

            Integer dropletImageId = dropletImage.getId();
            String dropletImageSlug = dropletImage.getSlug();
            Image availableImage = getDigitalOceanImageInfo(dropletImageId, dropletImageSlug, availableImagesById, availableImagesBySlug);

            if(availableImage == null) {
                throw new Exception("Droplet: " + dropletName + " has an invalid image specified - id: " + dropletImageId + ", slug: " + dropletImageSlug);
            }

            Region dropletRegion = droplet.getRegion();
            String dropletRegionSlug = dropletRegion.getSlug();
            boolean availableInRegion = false;

            for(String imageRegionSlug : availableImage.getRegions()) {
                if(dropletRegionSlug.equals(imageRegionSlug)) {
                    availableInRegion = true;
                    break;
                }
            }

            if(!availableInRegion) {
                throw new Exception("Droplet: " + dropletName + " specified region: " + dropletRegionSlug + " that is not available for image: " + dropletImageSlug);
            }

            dropletImage.setId(availableImage.getId());
            dropletImage.setSlug(availableImage.getSlug());
        });
    }

    private static Image getDigitalOceanImageInfo(Integer id, String slug, Map<Integer, Image> availableImagesById, Map<String, Image> availableImagesBySlug) throws Exception {
        Image image = null;

        if(id != null) {
            image = availableImagesById.get(id);

            if(image != null) {
                return image;
            }

            image = digitalOceanClient.getImageInfo(id);
        } else if(!StringUtils.isBlank(slug)) {
            image = availableImagesBySlug.get(slug);

            if(image != null) {
                return image;
            }

            image = digitalOceanClient.getImageInfo(slug);
        }

        if(image != null) {
            logDigitalOceanRateLimitRequestInfo("image", image);

            availableImagesById.put(image.getId(), image);
            availableImagesBySlug.put(image.getSlug(), image);
        }

        return image;
    }

    private static void evaluateLinodeKernels() throws Exception {
        JsonNode kernelsNode = linodeApi.availableKernels().asJson();
        KernelMapper kernelMapper = new KernelMapper(kernelsNode);
        Integer latest64BitKernelId = kernelMapper.findLatest(true);

        if(latest64BitKernelId == null) {
            throw new Exception("Could not find the latest Linode 64-bit kernel");
        }

        forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
            Integer configuredKernelId = linodeHostConfiguration.getKernelId();

            if(configuredKernelId == null) {
                linodeHostConfiguration.setKernelId(latest64BitKernelId);
            } else if(!configuredKernelId.equals(latest64BitKernelId)) {
                log.debug("Linode: " + linodeHostConfiguration.getLabel() + " is configured with outdated 64-bit kernelId: " + configuredKernelId + ", latest is: " + latest64BitKernelId);
            }
        });
    }

    private static void evaluateLinodeDistributions() throws Exception {
        JsonNode distributionsNode = linodeApi.availableDistributions().asJson();
        DistributionMapper distributionMapper = new DistributionMapper(distributionsNode);

        forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
            String configuredOsDistribution = linodeHostConfiguration.getOsDistribution();
            Integer configuredDistributionId = linodeHostConfiguration.getDistributionId();
            int distributionId = distributionMapper.getDistributionIdFromOs(configuredOsDistribution);

            if(configuredDistributionId == null) {
                linodeHostConfiguration.setDistributionId(distributionId);
            } else if(!configuredDistributionId.equals(distributionId)) {
                throw new Exception("Linode: " + linodeHostConfiguration.getLabel() + " has distributionId: " + distributionId + " but was configured with distribution: " + configuredOsDistribution + ", distributionId: " + configuredDistributionId);
            }
        });
    }

    private static void evaluateLinodePlans() throws Exception {
        JsonNode plansNode = linodeApi.availableLinodePlans().asJson();
        PlanMapper planMapper = new PlanMapper(plansNode);

        forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
            int configuredPlanRequiredRam = linodeHostConfiguration.getPlanRequiredRam();
            Integer configuredPlanId = linodeHostConfiguration.getPlanId();
            int planId = planMapper.getPlanIdFromRam(configuredPlanRequiredRam);

            if(configuredPlanId == null) {
                linodeHostConfiguration.setPlanId(planId);
            } else if(!configuredPlanId.equals(planId)) {
                throw new Exception("Linode: " + linodeHostConfiguration.getLabel() + " has planId: " + planId + " but was configured with planRequiredRam: " + configuredPlanRequiredRam + ", planId: " + configuredPlanId);
            }
        });
    }

    private static void evaluateLinodeDataCenters() throws Exception {
        JsonNode dataCentersNode = linodeApi.availableDataCenters().asJson();
        DataCenterMapper dataCenterMapper = new DataCenterMapper(dataCentersNode);

        forEachConfiguredLinode((hostConfiguration, linodeHostConfiguration) -> {
            String configuredDataCenterAbbreviation = linodeHostConfiguration.getDataCenterAbbreviation();
            Integer configuredDataCenterId = linodeHostConfiguration.getDataCenterId();
            int dataCenterId = dataCenterMapper.getDataCenterIdFromAbbr(configuredDataCenterAbbreviation);

            if(configuredDataCenterId == null) {
                linodeHostConfiguration.setDataCenterId(dataCenterId);
            } else if(!configuredDataCenterId.equals(dataCenterId)) {
                throw new Exception("Linode: " + linodeHostConfiguration.getLabel() + " has dataCenterId: " + dataCenterId + " but was configured with dataCenterAbbreviation: " + configuredDataCenterAbbreviation + ", dataCenterId: " + configuredDataCenterId);
            }
        });
    }

    private static void evaluateDigitalOceanRegions() throws Exception {
        Set<Region> availableRegions = new HashSet<>();
        int total;
        int pageNumber = 0;

        do {
            ++pageNumber;

            Regions regions = digitalOceanClient.getAvailableRegions(pageNumber);

            if(regions == null || regions.getRegions() == null || regions.getRegions().isEmpty()) {
                log.debug("No regions at page: " + pageNumber);
                break;
            }

            logDigitalOceanRequestInfo("regions", regions, pageNumber);

            total = regions.getMeta().getTotal();

            regions.getRegions().stream().forEach(availableRegions::add);
        } while(availableRegions.size() < total);

        forEachConfiguredDroplet((hostConfiguration, droplet) -> {
            String dropletName = droplet.getName();
            Region dropletRegion = droplet.getRegion();

            if(dropletRegion == null) {
                throw new Exception("Droplet: " + dropletName + " has no region specified");
            }

            String hostRegionSlug = dropletRegion.getSlug();

            if(StringUtils.isBlank(hostRegionSlug)) {
                throw new Exception("Droplet: " + dropletName + " has a region with no slug specified");
            }

            boolean hostRegionAvailable = false;

            for(Region availableRegion : availableRegions) {
                if(hostRegionSlug.equals(availableRegion.getSlug())) {
                    hostRegionAvailable = true;
                    break;
                }
            }

            if(!hostRegionAvailable) {
                throw new Exception("Droplet: " + dropletName + " requested region with slug: " + hostRegionSlug + " but none was available");
            }
        });
    }

    private static void logDigitalOceanRequestInfo(String category, Base base, int pageNumber) {
        logDigitalOceanRateLimitRequestInfo(category, base, pageNumber);

        String idStr = "for category: " + category + (pageNumber < 0 ? "" : (" at page: " + pageNumber));
        Meta meta = base.getMeta();
        Links links = base.getLinks();

        if(meta == null) {
            log.debug("No meta " + idStr);
        } else {
            log.debug("Meta " + idStr + " is " + meta.getTotal());
        }

        if(links == null) {
            log.debug("No links " + idStr);
        } else {
            Pages pages = links.getPages();

            if(pages == null) {
                log.debug("No link pages " + idStr);
            } else {
                log.debug("Pages " + idStr + " - first: " + pages.getFirst() + ", last: " + pages.getLast() + ", prev: " + pages.getPrev() + ", next: " + pages.getNext());
            }
        }
    }

    private static void logDigitalOceanRateLimitRequestInfo(String category, RateLimitBase rateLimitBase) {
        logDigitalOceanRateLimitRequestInfo(category, rateLimitBase, -1);
    }

    private static void logDigitalOceanRateLimitRequestInfo(String category, RateLimitBase rateLimitBase, int pageNumber) {
        String idStr = "for category: " + category + (pageNumber < 0 ? "" : (" at page: " + pageNumber));
        RateLimit rateLimit = rateLimitBase.getRateLimit();

        if(rateLimit == null) {
            log.debug("No rate limit " + idStr);
        } else {
            log.debug("Rate limit " + idStr + " is " + rateLimit.getLimit() + ", remaining: " + rateLimit.getRemaining() + ", reset: " + (rateLimit.getReset() == null ? null : Config.DATE_FORMAT.format(rateLimit.getReset())));
        }
    }

    private static void forEachConfiguredLinode(ConfiguredLinodeAction configuredLinodeAction) throws Exception {
        for(HostConfiguration hostConfiguration : hostConfigByPriorityMap.values()) {
            LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration = hostConfiguration.getLinodeHostConfiguration();

            if(linodeHostConfiguration != null) {
                configuredLinodeAction.execute(hostConfiguration, linodeHostConfiguration);
            }
        }
    }

    private static void forEachConfiguredDroplet(ConfiguredDropletAction configuredDropletAction) throws Exception {
        for(HostConfiguration hostConfiguration : hostConfigByPriorityMap.values()) {
            Droplet droplet = hostConfiguration.getDroplet();

            if(droplet != null) {
                configuredDropletAction.execute(hostConfiguration, droplet);
            }
        }
    }
}
