package com.exesus.deployer;

public enum Provider {
    DIGITAL_OCEAN, LINODE;
}
