package com.exesus.deployer;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public final class SshUtil {
    private static final Logger log = LoggerFactory.getLogger(SshUtil.class);

    private static final int BUFFER_SIZE = 1024;
    private static final byte[] NULL_BYTES = new byte[]{0};

    private SshUtil() {
    }

    public static int execute(Session session, String command) throws Exception {
        return executeWithResult(session, command).getExitStatus();
    }

    public static int execute(Session session, Logger logger, String command) throws Exception {
        return executeWithResult(session, logger, command).getExitStatus();
    }

    public static SshCommandResult executeWithResult(Session session, String command) throws Exception {
        return executeWithResult(session, null, command);
    }

    public static SshCommandResult executeWithResult(Session session, Logger logger, String command) throws Exception {
        SshCommandResult result = new SshCommandResult();
        ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

        channelExec.setCommand(command);

        try(InputStream inputStream = channelExec.getInputStream()) {
            try(InputStream errorStream = channelExec.getErrStream()) {
                if(logger != null) {
                    log.debug("Command: '" + command + "' executing");
                }

                channelExec.connect();

                try {
                    StringBuilder stringBuilder = (logger == null ? new StringBuilder() : null);
                    byte[] buffer = new byte[BUFFER_SIZE];

                    while(true) {
                        while(inputStream.available() > 0) {
                            int count = inputStream.read(buffer, 0, buffer.length);

                            if(count < 0) {
                                break;
                            }

                            String str = new String(buffer, 0, count);

                            if(stringBuilder != null) {
                                stringBuilder.append(str);
                            }

                            if(logger != null) {
                                for(String crPart : str.split("\r")) {
                                    for(String lfPart : crPart.split("\n")) {
                                        logger.debug(lfPart);
                                    }
                                }
                            }
                        }

                        while(errorStream.available() > 0) {
                            int count = errorStream.read(buffer, 0, buffer.length);

                            if(count < 0) {
                                break;
                            }

                            String str = new String(buffer, 0, count);

                            if(stringBuilder != null) {
                                stringBuilder.append(str);
                            }

                            if(logger != null) {
                                for(String crPart : str.split("\r")) {
                                    for(String lfPart : crPart.split("\n")) {
                                        logger.warn(lfPart);
                                    }
                                }
                            }
                        }

                        if(channelExec.isClosed()) {
                            int exitStatus = channelExec.getExitStatus();
                            String output = (stringBuilder == null ? null : stringBuilder.toString());

                            log.debug("Command: '" + command + "' exited with code: " + exitStatus + (output == null ? "" : ", output: " + output));
                            result.setExitStatus(exitStatus);
                            result.setOutput(output);

                            return result;
                        }
                    }
                } finally {
                    try {
                        channelExec.disconnect();
                    } catch(Exception e) {
                        log.warn("Error disconnecting command exec channel from host: " + session.getHost(), e);
                    }
                }
            }
        }
    }

    public static Session createSession(File identityFile, String passphrase, String user, String host, int port) throws Exception {
        JSch jsch = new JSch();

        jsch.addIdentity(identityFile.getAbsolutePath(), passphrase);

        Properties config = new Properties();

        config.put("StrictHostKeyChecking", "no");
        config.put("UserKnownHostsFile", "/dev/null");

        Session session = jsch.getSession(user, host, port);

        session.setConfig(config);
        session.connect();

        return session;
    }

    public static boolean scp(Session session, File sourceFile, String targetPath, String mode) throws Exception {
        String scpCommand = "scp -p -t " + targetPath;
        ChannelExec channelExec = (ChannelExec)session.openChannel("exec");

        channelExec.setCommand(scpCommand);

        try(OutputStream outputStream = channelExec.getOutputStream()) {
            try(InputStream inputStream = channelExec.getInputStream()) {
                channelExec.connect();

                try {
                    if(!checkScpAcknowledgement(inputStream)) {
                        return false;
                    }

                    long sourceLastModifiedSeconds = TimeUnit.MILLISECONDS.toSeconds(sourceFile.lastModified());
                    Path sourceFilePath = Paths.get(sourceFile.getAbsolutePath());
                    BasicFileAttributes sourceBasicFileAttributes = Files.readAttributes(sourceFilePath, BasicFileAttributes.class);
                    long sourceLastAccessTimeSeconds = sourceBasicFileAttributes.lastAccessTime().to(TimeUnit.SECONDS);
                    String timestampCommand = "T" + sourceLastModifiedSeconds + " 0 " + sourceLastAccessTimeSeconds + " 0\n";

                    outputStream.write(timestampCommand.getBytes());
                    outputStream.flush();

                    if(!checkScpAcknowledgement(inputStream)) {
                        return false;
                    }

                    String modeCommand = "C" + mode + " " + sourceFile.length() + " " + sourceFile.getName() + "\n";

                    outputStream.write(modeCommand.getBytes());
                    outputStream.flush();

                    if(!checkScpAcknowledgement(inputStream)) {
                        return false;
                    }

                    try(FileInputStream fileInputStream = new FileInputStream(sourceFile)) {
                        byte[] buffer = new byte[BUFFER_SIZE];

                        while(true) {
                            int length = fileInputStream.read(buffer, 0, buffer.length);

                            if(length <= 0) {
                                break;
                            }

                            outputStream.write(buffer, 0, length);
                        }
                    }

                    outputStream.write(NULL_BYTES, 0, 1);
                    outputStream.flush();

                    return checkScpAcknowledgement(inputStream);
                } finally {
                    try {
                        channelExec.disconnect();
                    } catch(Exception e) {
                        log.warn("Error disconnecting scp exec channel from host: " + session.getHost(), e);
                    }
                }
            }
        }
    }

    private static boolean checkScpAcknowledgement(InputStream inputStream) throws Exception {
        int firstByte = inputStream.read();

        switch(firstByte) {
            case 0:
                return true;
            case 1:
            case 2:
                StringBuilder errorStringBuilder = new StringBuilder();
                int nextByte;

                do {
                    nextByte = inputStream.read();
                    errorStringBuilder.append((char)nextByte);
                } while(nextByte != '\n');

                String errorString = errorStringBuilder.toString();

                log.error("Scp acknowledgement " + (firstByte == 2 ? "fatal " : "") + "error: " + errorString);
                break;
        }

        return false;
    }

    public static class SshCommandResult {
        private int exitStatus = 1;
        private String output = null;

        public int getExitStatus() {
            return exitStatus;
        }

        public void setExitStatus(int exitStatus) {
            this.exitStatus = exitStatus;
        }

        public String getOutput() {
            return output;
        }

        public void setOutput(String output) {
            this.output = output;
        }
    }
}
