package com.exesus.deployer;

import freemarker.cache.FileTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.io.Charsets;

import java.io.*;
import java.util.*;

public final class TemplateUtil {
    private static final Map<String, Configuration> freeMarkerConfigMap = Collections.synchronizedMap(new HashMap<>());

    private TemplateUtil() {
    }

    public static String merge(File templateFile, Object data) throws Exception {
        Template template = getTemplate(templateFile);

        try(StringWriter stringWriter = new StringWriter()) {
            template.process(data, stringWriter);

            return stringWriter.toString();
        }
    }

    public static void mergeToFile(File templateFile, File targetFile, Object data) throws Exception {
        Template template = getTemplate(templateFile);

        try(FileWriter fileWriter = new FileWriter(targetFile)) {
            template.process(data, fileWriter);
        }
    }

    private static Template getTemplate(File templateFile) throws IOException {
        File parentDir = templateFile.getParentFile();
        String parentPath = parentDir.getAbsolutePath();
        Configuration freeMarkerConfiguration = freeMarkerConfigMap.get(parentPath);

        if(freeMarkerConfiguration == null) {
            freeMarkerConfiguration = new Configuration(Configuration.VERSION_2_3_23);

            freeMarkerConfiguration.setTemplateLoader(new FileTemplateLoader(parentDir));
            freeMarkerConfiguration.setDefaultEncoding(Charsets.UTF_8.name());
            freeMarkerConfiguration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

            freeMarkerConfigMap.put(parentPath, freeMarkerConfiguration);
        }

        return freeMarkerConfiguration.getTemplate(templateFile.getName());
    }
}
