package com.exesus.deployer;

import com.exesus.deployer.config.LinodeHostConfig;
import com.fasterxml.jackson.databind.JsonNode;
import com.myjeeva.digitalocean.pojo.Droplet;
import com.myjeeva.digitalocean.pojo.Network;
import com.myjeeva.digitalocean.pojo.Networks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.co.solong.linode4j.Linode;

import java.util.List;

public class Utils {
    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    private Utils() {
    }

    public static String getDropletDefaultIpv4(Droplet droplet) {
        Networks networks = droplet.getNetworks();
        List<Network> ipv4Networks = (networks == null ? null : networks.getVersion4Networks());
        Network ipv4Network = ((ipv4Networks == null || ipv4Networks.isEmpty()) ? null : ipv4Networks.get(0));

        return (ipv4Network == null ? null : ipv4Network.getIpAddress());
    }

    public static String getLinodeDefaultIpv4(Linode linodeApi, LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration) throws Exception {
        Integer linodeId = linodeHostConfiguration.getLinodeId();
        String linodeLabel = linodeHostConfiguration.getLabel();
        String hostName = linodeLabel.replace('_', '.');
        JsonNode ipListNode = linodeApi.listIP().withLinodeId(linodeId).asJson();
        JsonNode ipListDataNode = (ipListNode == null ? null : ipListNode.get("DATA"));
        String firstPublicIp = null;
        int publicIpCount = 0;

        if(ipListDataNode != null) {
            for(JsonNode ipAddressNode : ipListDataNode) {
                int isPublic = ipAddressNode.get("ISPUBLIC").asInt();

                if(isPublic != 1) {
                    continue;
                }

                publicIpCount++;

                String ipAddress = ipAddressNode.get("IPADDRESS").asText();
                String reverseDnsName = ipAddressNode.get("RDNS_NAME").asText();

                if(reverseDnsName.equals(hostName)) {
                    return ipAddress;
                }

                if(firstPublicIp == null) {
                    firstPublicIp = ipAddress;
                }
            }
        }

        if(publicIpCount > 1) {
            throw new Exception("Host: " + hostName + ", linodeId: " + linodeId + " has " + publicIpCount + " public IP addresses, can't determine primary");
        }

        return firstPublicIp;
    }

    public static void sleepQuietly(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch(InterruptedException ie) {
            log.trace("Interrupted sleep of " + milliseconds + " milliseconds", ie);
        }
    }
}
