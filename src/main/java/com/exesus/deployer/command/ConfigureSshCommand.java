package com.exesus.deployer.command;

import com.exesus.deployer.*;
import com.exesus.deployer.config.Config;
import com.exesus.deployer.config.ProvisionScript;
import com.google.common.collect.ImmutableMap;
import com.jcraft.jsch.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.UUID;

public class ConfigureSshCommand extends ProvisionScriptCommand {
    private static final Logger log = LoggerFactory.getLogger(ConfigureSshCommand.class);

    private final String host;
    private final int defaultPort;
    private final String initialUser;
    private final int maxRetries;
    private final File privateKeyFile;
    private final File publicKeyFile;
    private final long retryInterval;

    public ConfigureSshCommand(String host, int defaultPort, String initialUser, File privateKeyFile, File publicKeyFile) throws Exception {
        super(ProvisionScript.SSH, host);

        this.host = host;
        this.defaultPort = defaultPort;
        this.initialUser = initialUser;
        this.maxRetries = Config.getInt(Config.Key.CONFIGURE_SSH_RETRY_LIMIT);
        this.privateKeyFile = privateKeyFile;
        this.publicKeyFile = publicKeyFile;
        this.retryInterval = Config.getLong(Config.Key.CONFIGURE_SSH_RETRY_INTERVAL);
    }

    public Boolean run() {
        String randomUuid = UUID.randomUUID().toString();
        String remotePublicKeyPath = "/tmp/" + randomUuid + ".pub";

        try {
            Session session = null;
            int attemptCount;

            for(attemptCount = 1; attemptCount <= maxRetries; attemptCount++) {
                if(attemptCount > 1) {
                    Utils.sleepQuietly(retryInterval);
                }

                try {
                    session = SshUtil.createSession(privateKeyFile, null, initialUser, host, defaultPort);
                    break;
                } catch(Exception e) {
                    log.warn("Connection attempt #" + attemptCount + " to host: " + host + " failed", e);
                }
            }

            if(session == null) {
                throw new RuntimeException("Failed to connect to host: " + host + " after " + (attemptCount) + " attempts");
            }

            try {
                if(!SshUtil.scp(session, publicKeyFile, remotePublicKeyPath, "0600")) {
                    throw new RuntimeException("Failed to scp public key: " + publicKeyFile.getAbsolutePath() + " to host: " + host);
                }

                setSession(session);
                setTemplateData(ImmutableMap.of(
                    "key_path", remotePublicKeyPath,
                    "ssh_port", Integer.toString(Config.getInt(Config.Key.SSH_PORT)),
                    "ssh_user", Config.getString(Config.Key.SSH_USER)
                ));

                return super.run();
            } finally {
                try {
                    session.disconnect();
                } catch(Exception e) {
                    log.warn("Error disconnecting session to host: " + host, e);
                }
            }
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error configured ssh access on host: " + host, e);
        }
    }
}
