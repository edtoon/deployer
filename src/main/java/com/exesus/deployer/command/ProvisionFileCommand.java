package com.exesus.deployer.command;

import com.exesus.deployer.config.ProvisionFile;
import com.exesus.deployer.SshUtil;
import com.jcraft.jsch.Session;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;

import java.io.File;

public class ProvisionFileCommand extends HystrixCommand<Boolean> {
    private final ProvisionFile provisionFile;
    private final String hostPath;
    private final Session session;

    public ProvisionFileCommand(ProvisionFile provisionFile, String hostPath, Session session) throws Exception {
        super(HystrixCommandGroupKey.Factory.asKey(session.getHost()), (int)provisionFile.getExecutionTimeoutInMilliseconds());

        this.provisionFile = provisionFile;
        this.hostPath = hostPath;
        this.session = session;
    }

    public Boolean getFallback() {
        return false;
    }

    public Boolean run() {
        File localFile = provisionFile.getLocalFile();
        String localFilePath = localFile.getAbsolutePath();
        String hostDir = hostPath.substring(0, hostPath.lastIndexOf('/'));
        String host = session.getHost();
        String user = session.getUserName();

        try {
            if(SshUtil.execute(session, "sudo mkdir -p " + hostDir) != 0) {
                throw new RuntimeException("Unable to make directory for file: " + hostPath + " on host: " + host);
            }

            if(SshUtil.execute(session, "sudo chown " + user + ":" + user + " " + hostDir) != 0) {
                throw new RuntimeException("Unable to chown path: " + hostDir + " on host: " + host);
            }

            SshUtil.SshCommandResult duResult = SshUtil.executeWithResult(session, "sudo du -b " + hostPath);

            if(duResult.getExitStatus() == 0) {
                String duOutput = duResult.getOutput();
                int fileSize = Integer.parseInt(duOutput.substring(0, duOutput.indexOf('\t')));

                if(fileSize == localFile.length()) {
                    return true;
                }

                SshUtil.execute(session, "sudo rm -f " + hostPath);
            }

            if(!SshUtil.scp(session, localFile, hostPath, "0600")) {
                throw new RuntimeException("Failed to scp file: " + localFilePath + " to host: " + host + ", path: " + hostPath);
            }

            if(SshUtil.execute(session, "sudo chown " + user + ":" + user + " " + hostPath) != 0) {
                throw new RuntimeException("Unable to chown path: " + hostPath + " on host: " + host);
            }

            return true;
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error provisioning file: " + localFilePath + " on host: " + host, e);
        }
    }
}
