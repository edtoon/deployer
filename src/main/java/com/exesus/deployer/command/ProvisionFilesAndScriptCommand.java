package com.exesus.deployer.command;

import com.exesus.deployer.config.ProvisionScript;
import com.exesus.deployer.SshUtil;
import com.jcraft.jsch.Session;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.util.Set;

public class ProvisionFilesAndScriptCommand extends ProvisionScriptCommand {
    private final Set<Pair<File, String>> provisionFilesAtPaths;

    public ProvisionFilesAndScriptCommand(Set<Pair<File, String>> provisionFilesAtPaths, ProvisionScript provisionScript, Session session) throws Exception {
        this(provisionFilesAtPaths, provisionScript, session, null);
    }

    public ProvisionFilesAndScriptCommand(Set<Pair<File, String>> filesAndPaths, ProvisionScript provisionScript, Session session, Object templateData) throws Exception {
        super(provisionScript, session, templateData);

        this.provisionFilesAtPaths = filesAndPaths;
    }

    public Boolean run() {
        Session session = getSession();
        String host = session.getHost();

        try {
            for(Pair<File, String> provisionFileAtPath : provisionFilesAtPaths) {
                File localFile = provisionFileAtPath.getKey();
                String remotePath = provisionFileAtPath.getValue();

                if(SshUtil.execute(session, "mkdir -p " + remotePath.substring(0, remotePath.lastIndexOf('/'))) != 0) {
                    throw new RuntimeException("Failed to make directory for remote file: " + remotePath + " on host: " + host);
                } else if(!SshUtil.scp(session, localFile, provisionFileAtPath.getValue(), "0600")) {
                    throw new RuntimeException("Failed to scp file: " + localFile.getAbsolutePath() + " to host: " + host);
                }
            }
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error provisioning files and script: " + getProvisionScript().name() + " on host: " + host, e);
        }

        return super.run();
    }
}
