package com.exesus.deployer.command;

import com.exesus.deployer.config.Config;
import com.exesus.deployer.config.ProvisionImage;
import com.exesus.deployer.SshUtil;
import com.jcraft.jsch.Session;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ProvisionImageCommand extends HystrixCommand<Boolean> {
    private static final Logger log = LoggerFactory.getLogger(ProvisionImageCommand.class);

    private final ProvisionImage provisionImage;
    private final Session session;

    public ProvisionImageCommand(ProvisionImage provisionImage, Session session) throws Exception {
        super(HystrixCommandGroupKey.Factory.asKey(session.getHost()), (int)provisionImage.getExecutionTimeoutInMilliseconds());

        this.provisionImage = provisionImage;
        this.session = session;
    }

    public Boolean getFallback() {
        return false;
    }

    public Boolean run() {
        String imageName = provisionImage.getImageName();
        String imagePath = provisionImage.getImagePath();
        File imageDir = new File(Config.EXESUS_DIR, imagePath);
        String hostImageDir = Config.getString(Config.Key.HOST_IMAGE_DIR) + "/" + provisionImage.name().toLowerCase();
        String host = session.getHost();
        String user = session.getUserName();

        try {
            if(SshUtil.execute(session, "sudo mkdir -p " + hostImageDir) != 0) {
                throw new RuntimeException("Unable to make image directory: " + hostImageDir + " on host: " + host);
            }

            if(SshUtil.execute(session, "sudo chown -R " + user + ":" + user + " " + hostImageDir) != 0) {
                throw new RuntimeException("Unable to chown image directory: " + hostImageDir + " on host: " + host);
            }

            boolean hasBuildScript = false;
            File[] files = imageDir.listFiles();

            if(files != null) {
                for(File file : files) {
                    if(file.isDirectory()) {
                        throw new RuntimeException("Don't know how to provision images with subdirectories right now");
                    }

                    String mode = (file.canExecute() ? "0700" : "0600");
                    String fileName = file.getName();

                    if("build.sh".equals(fileName)) {
                        hasBuildScript = true;
                    }

                    if(!SshUtil.scp(session, file, hostImageDir + "/" + file.getName(), mode)) {
                        throw new RuntimeException("Unable to upload file: " + file.getAbsolutePath() + " to host: " + host);
                    }
                }
            }

            String command = "sudo " + (
                hasBuildScript ?
                    (hostImageDir + "/build.sh") :
                    ("docker build -t " + imageName + ":" + provisionImage.getImageTag() + " " + hostImageDir)
            );

            if(SshUtil.execute(session, log, command) != 0) {
                throw new RuntimeException("Unable to build image: " + imagePath + " on host: " + host);
            }

            return true;
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error provisioning image: " + imagePath + " on host: " + host, e);
        }
    }
}
