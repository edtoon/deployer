package com.exesus.deployer.command;

import com.exesus.deployer.config.Config;
import com.exesus.deployer.config.ProvisionScript;
import com.exesus.deployer.TemplateUtil;
import com.jcraft.jsch.Session;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class ProvisionScriptCommand extends ScpAndExecuteCommand {
    private final ProvisionScript provisionScript;

    private Object templateData = null;

    public ProvisionScriptCommand(ProvisionScript provisionScript, Session session) throws Exception {
        this(provisionScript, session, null);
    }

    public ProvisionScriptCommand(ProvisionScript provisionScript, Session session, Object templateData) throws Exception {
        super(null, null, true, session, provisionScript.getExecutionTimeoutInMilliseconds());

        this.provisionScript = provisionScript;
        this.templateData = templateData;
    }

    public ProvisionScriptCommand(ProvisionScript provisionScript, String host) throws Exception {
        this(provisionScript, host, null);
    }

    public ProvisionScriptCommand(ProvisionScript provisionScript, String host, Object templateData) throws Exception {
        super(null, null, true, host, provisionScript.getExecutionTimeoutInMilliseconds());

        this.provisionScript = provisionScript;
        this.templateData = templateData;
    }

    public Boolean run() {
        String scriptName = provisionScript.name();
        boolean mergeScript = (templateData != null);
        File provisionScriptFile = provisionScript.getScriptFile();
        File scriptFile = provisionScriptFile;

        try {
            if(mergeScript) {
                scriptFile = File.createTempFile(scriptName, ".sh", Config.TEMP_DIR);

                TemplateUtil.mergeToFile(provisionScriptFile, scriptFile, templateData);
            }

            setLocalFile(scriptFile);
            setRemoteScriptPath("/tmp/" + scriptFile.getName());

            return super.run();
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error executing script: " + scriptName + " on host: " + getSession().getHost(), e);
        } finally {
            if(mergeScript) {
                FileUtils.deleteQuietly(scriptFile);
            }
        }
    }

    public ProvisionScript getProvisionScript() {
        return provisionScript;
    }

    public Object getTemplateData() {
        return templateData;
    }

    public void setTemplateData(Object templateData) {
        this.templateData = templateData;
    }
}

