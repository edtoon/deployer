package com.exesus.deployer.command;

import com.exesus.deployer.config.Config;
import com.exesus.deployer.Utils;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;

public class ReachablePortCommand extends HystrixCommand<Integer> {
    private static final Logger log = LoggerFactory.getLogger(ReachablePortCommand.class);

    private final String checkHost;
    private final int[] checkPorts;
    private final int maxRetries;
    private final long retryInterval;

    public ReachablePortCommand(String checkHost, int[] checkPorts, int maxRetries, long retryInterval) {
        super(HystrixCommandGroupKey.Factory.asKey(checkHost), (int)Config.getLong(Config.Key.REACHABLE_PORT_CHECK_TIMEOUT));

        this.checkHost = checkHost;
        this.checkPorts = checkPorts;
        this.maxRetries = maxRetries;
        this.retryInterval = retryInterval;
    }

    public Integer getFallback() {
        return null;
    }

    public Integer run() {
        Exception lastException = null;

        for(int attemptCount = 1; attemptCount <= maxRetries; attemptCount++) {
            if(attemptCount > 1) {
                Utils.sleepQuietly(retryInterval);
            }

            for(int checkPort : checkPorts) {
                try(Socket defaultSshSocket = new Socket(checkHost, checkPort)) {
                    if(defaultSshSocket.isConnected()) {
                        return checkPort;
                    }
                } catch(Exception e) {
                    log.debug("Check #" + attemptCount + " of host: " + checkHost + ", port: " + checkPort + " failed - " + e.getMessage());

                    lastException = e;
                }
            }
        }

        throw new RuntimeException("Failed to find a reachable port for host: " + checkHost, lastException);
    }
}
