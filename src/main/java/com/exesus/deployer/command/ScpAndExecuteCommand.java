package com.exesus.deployer.command;

import com.exesus.deployer.SshUtil;
import com.jcraft.jsch.Session;
import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class ScpAndExecuteCommand extends HystrixCommand<Boolean> {
    private static final Logger log = LoggerFactory.getLogger(ScpAndExecuteCommand.class);

    private File localFile = null;
    private String remoteScriptPath = null;
    private boolean removeAfterRunning = false;
    private Session session = null;
    private String host = null;

    public ScpAndExecuteCommand(File localFile, String remoteScriptPath, boolean removeRemoteAfterRunning, Session session, long executionTimeoutInMilliseconds) {
        this(localFile, remoteScriptPath, removeRemoteAfterRunning, session.getHost(), (int)executionTimeoutInMilliseconds);

        this.session = session;
    }

    public ScpAndExecuteCommand(File localFile, String remoteScriptPath, boolean removeAfterRunning, String host, long executionTimeoutInMilliseconds) {
        super(HystrixCommandGroupKey.Factory.asKey(host), (int)executionTimeoutInMilliseconds);

        this.localFile = localFile;
        this.remoteScriptPath = remoteScriptPath;
        this.removeAfterRunning = removeAfterRunning;
        this.host = host;
    }

    public Boolean getFallback() {
        return false;
    }

    public Boolean run() {
        String localFilePath = localFile.getAbsolutePath();

        try {
            if(!SshUtil.scp(session, localFile, remoteScriptPath, "0700")) {
                throw new RuntimeException("Failed to scp file: " + localFilePath + " to host: " + host + ", path: " + remoteScriptPath);
            } else if(SshUtil.execute(session, log, remoteScriptPath + (removeAfterRunning ? (" && rm -f " + remoteScriptPath) : "")) != 0) {
                throw new RuntimeException("Failed to execute script: " + remoteScriptPath + " on host: " + host);
            }
        } catch(RuntimeException re) {
            throw re;
        } catch(Exception e) {
            throw new RuntimeException("Error executing script: " + localFilePath + " on host: " + host, e);
        }

        return true;
    }

    public File getLocalFile() {
        return localFile;
    }

    protected void setLocalFile(File localFile) {
        this.localFile = localFile;
    }

    public String getRemoteScriptPath() {
        return remoteScriptPath;
    }

    protected void setRemoteScriptPath(String remoteScriptPath) {
        this.remoteScriptPath = remoteScriptPath;
    }

    public boolean getRemoveAfterRunning() {
        return removeAfterRunning;
    }

    protected void setRemoveAfterRunning(boolean removeAfterRunning) {
        this.removeAfterRunning = removeAfterRunning;
    }

    public Session getSession() {
        return session;
    }

    protected void setSession(Session session) {
        this.session = session;
    }
}
