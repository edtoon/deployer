package com.exesus.deployer.config;

import com.netflix.config.DynamicPropertyFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

public class Config {
    public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss z");
    public static final File EXESUS_DIR = new File(System.getenv("EXESUS_DIR"));
    public static final File TEMP_DIR = new File(System.getProperty("java.io.tmpdir"));
    public static final File USER_HOME_DIR = new File(System.getProperty("user.home"));
    private static final int UNRECOGNIZED_INT_VALUE = Integer.MIN_VALUE;
    private static final long UNRECOGNIZED_LONG_VALUE = Long.MIN_VALUE;

    private Config() {
    }

    public enum KeyType {
        STRING, FILE, INT, LONG
    }

    public enum Key {
        CAM_DB_NAME("CAM_DB_NAME", KeyType.STRING),
        CAM_DB_PASSWORD("CAM_DB_PASSWORD", KeyType.STRING),
        CAM_DB_USER("CAM_DB_USER", KeyType.STRING),
        CAM_GIT_TIMEOUT("CAM_GIT_TIMEOUT", KeyType.LONG, TimeUnit.MINUTES.toMillis(1)),
        CAM_RSYNC_TIMEOUT("CAM_RSYNC_TIMEOUT", KeyType.LONG, TimeUnit.MINUTES.toMillis(1)),
        CAM_SQL_GZ("CAM_SQL_GZ", KeyType.FILE),
        CAM_SRC_DIR("CAM_SRC_DIR", KeyType.STRING),
        CAM_UPLOADS_TGZ("CAM_UPLOADS_TGZ", KeyType.FILE),
        CONFIGURE_SSH_RETRY_INTERVAL("CONFIGURE_SSH_RETRY_INTERVAL", KeyType.LONG, TimeUnit.SECONDS.toMillis(30)),
        CONFIGURE_SSH_RETRY_LIMIT("CONFIGURE_SSH_RETRY_LIMIT", KeyType.INT, 10),
        CROSSTALK_PASSPHRASE("CROSSTALK_PASSPHRASE", KeyType.STRING),
        CROSSTALK_PUB_KEY("CROSSTALK_PUB_KEY", KeyType.FILE),
        CROSSTALK_PVT_KEY("CROSSTALK_PVT_KEY", KeyType.FILE),
        CROSSTALK_USER("CROSSTALK_USER", KeyType.STRING),
        BITBUCKET_PASSPHRASE("BITBUCKET_PASSPHRASE", KeyType.STRING),
        BITBUCKET_PVT_KEY("BITBUCKET_PVT_KEY", KeyType.FILE),
        DEPLOY_AWAIT_DROPLET_ACTIVE_INTERVAL("DEPLOY_AWAIT_DROPLET_ACTIVE_INTERVAL", KeyType.LONG, TimeUnit.SECONDS.toMillis(30)),
        DEPLOY_CHECK_SSH_PORT_RETRY_INTERVAL("DEPLOY_CHECK_SSH_PORT_RETRY_INTERVAL", KeyType.LONG, TimeUnit.SECONDS.toMillis(30)),
        DEPLOY_CHECK_SSH_PORT_RETRY_LIMIT("DEPLOY_CHECK_SSH_PORT_RETRY_LIMIT", KeyType.INT, 10),
        DEPLOY_LINODE_WAIT_PENDING_JOBS("DEPLOY_LINODE_WAIT_PENDING_JOBS", KeyType.LONG, TimeUnit.SECONDS.toMillis(10)),
        DEPLOY_WAIT_AFTER_SSH_CONFIG("DEPLOY_WAIT_AFTER_SSH_CONFIG", KeyType.LONG, 1500L),
        DIGITAL_OCEAN_KEY_ID("DO_KEY_ID", KeyType.INT),
        DIGITAL_OCEAN_PUB_KEY("DO_PUB_KEY", KeyType.FILE),
        DIGITAL_OCEAN_PVT_KEY("DO_PVT_KEY", KeyType.FILE),
        DIGITAL_OCEAN_TOKEN("DO_TOKEN", KeyType.STRING),
        LINODE_API_KEY("LINODE_API_KEY", KeyType.STRING),
        LINODE_PUB_KEY("LINODE_PUB_KEY", KeyType.FILE),
        LINODE_PVT_KEY("LINODE_PVT_KEY", KeyType.FILE),
        MYSQL_DATA_DIR("MYSQL_DATA_DIR", KeyType.STRING),
        MYSQL_ROOT_PASSWORD("MYSQL_ROOT_PASSWORD", KeyType.STRING),
        HOST_FILE_DIR("HOST_FILE_DIR", KeyType.STRING, "/opt/exesus/files"),
        HOST_HUB_DIR("HOST_HUB_DIR", KeyType.STRING, "/opt/exesus/hub"),
        HOST_IMAGE_DIR("HOST_IMAGE_DIR", KeyType.STRING, "/opt/exesus/images"),
        HOST_PROVISION_DIR("HOST_PROVISION_DIR", KeyType.STRING, "/opt/exesus/provision"),
        HOST_TMP_DIR("HOST_TMP_DIR", KeyType.STRING, "/opt/exesus/tmp"),
        REACHABLE_PORT_CHECK_TIMEOUT("REACHABLE_PORT_CHECK_TIMEOUT", KeyType.LONG, TimeUnit.MINUTES.toMillis(10)),
        SSH_PORT("SSH_PORT", KeyType.INT),
        SSH_USER("SSH_USER", KeyType.STRING);

        private final String propertyName;
        private final KeyType type;
        private final boolean hasDefaultValue;
        private final Object defaultValue;

        Key(String propertyName, KeyType type) {
            this.propertyName = propertyName;
            this.type = type;
            hasDefaultValue = false;
            defaultValue = null;
        }

        Key(String propertyName, KeyType type, String defaultString) {
            if(KeyType.STRING != type) {
                throw new RuntimeException("String default value specified for " + type.name() + " property: " + propertyName);
            }

            this.propertyName = propertyName;
            this.type = type;

            hasDefaultValue = true;
            defaultValue = defaultString;
        }

        Key(String propertyName, KeyType type, Integer defaultInteger) {
            String typeName = type.name();

            if(KeyType.INT != type) {
                throw new RuntimeException("Integer default value specified for " + typeName + " property: " + propertyName);
            }

            this.propertyName = propertyName;
            this.type = type;

            if(defaultInteger == null) {
                throw new RuntimeException("Null default value specified for " + typeName + " property: " + propertyName);
            }

            hasDefaultValue = true;
            defaultValue = defaultInteger;
        }

        Key(String propertyName, KeyType type, Long defaultLong) {
            String typeName = type.name();

            if(KeyType.LONG != type) {
                throw new RuntimeException("Long default value specified for " + typeName + " property: " + propertyName);
            }

            this.propertyName = propertyName;
            this.type = type;

            if(defaultLong == null) {
                throw new RuntimeException("Null default value specified for " + typeName + " property: " + propertyName);
            }

            hasDefaultValue = true;
            defaultValue = defaultLong;
        }

        public String getPropertyName() {
            return propertyName;
        }

        public KeyType getType() {
            return type;
        }

        public boolean getHasDefaultValue() {
            return hasDefaultValue;
        }

        public Object getDefaultValue() {
            if(!hasDefaultValue) {
                throw new RuntimeException("Tried to get default value for property: " + propertyName + " but no default was specified");
            }

            return defaultValue;
        }
    }

    public static String getString(Key key) {
        verifyKeyType(key, KeyType.STRING);

        String defaultValue = (key.hasDefaultValue ? (String)key.getDefaultValue() : null);

        return DynamicPropertyFactory.getInstance().getStringProperty(key.getPropertyName(), defaultValue).get();
    }

    public static File getFile(Key key) {
        verifyKeyType(key, KeyType.FILE);

        String value = DynamicPropertyFactory.getInstance().getStringProperty(key.getPropertyName(), null).get();

        if(StringUtils.isBlank(value)) {
            throw new RuntimeException("Found empty or no value for " + key.getType().name() + " property: " + key.getPropertyName());
        } else if(value.startsWith("~/")) {
            return new File(USER_HOME_DIR, value.substring(1));
        }

        return new File(value);
    }

    public static int getInt(Key key) {
        verifyKeyType(key, KeyType.INT);

        String propertyName = key.getPropertyName();
        Integer defaultValue = (key.hasDefaultValue ? (Integer)key.getDefaultValue() : UNRECOGNIZED_INT_VALUE);
        int value = DynamicPropertyFactory.getInstance().getIntProperty(propertyName, defaultValue).get();

        if(value == UNRECOGNIZED_INT_VALUE) {
            throw new RuntimeException("Found invalid value for " + key.getType().name() + " property: " + propertyName + ", property is likely undefined");
        }

        return value;
    }

    public static long getLong(Key key) {
        verifyKeyType(key, KeyType.LONG);

        String propertyName = key.getPropertyName();
        Long defaultValue = (key.hasDefaultValue ? (Long)key.getDefaultValue() : UNRECOGNIZED_LONG_VALUE);
        long value = DynamicPropertyFactory.getInstance().getLongProperty(propertyName, defaultValue).get();

        if(value == UNRECOGNIZED_LONG_VALUE) {
            throw new RuntimeException("Found invalid value for " + key.getType().name() + " property: " + propertyName + ", property is likely undefined");
        }

        return value;
    }

    private static void verifyKeyType(Key key, KeyType expectedType) {
        KeyType keyType = key.getType();

        if(expectedType != keyType) {
            throw new RuntimeException("Tried to get " + keyType.name() + " property: " + key.getPropertyName() + " as " + expectedType.name());
        }
    }
}
