package com.exesus.deployer.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.myjeeva.digitalocean.pojo.Droplet;
import com.myjeeva.digitalocean.pojo.Image;
import com.myjeeva.digitalocean.pojo.Key;
import com.myjeeva.digitalocean.pojo.Region;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.Set;

public class DigitalOceanHostConfig {
    public static final String DIGITAL_OCEAN_HOST_CAM_DEV = "www.dev.conflictarea.com";

    private static final Image DIGITAL_OCEAN_IMAGE_DEBIAN_8_X64 = new Image("debian-8-x64");
    private static final Region DIGITAL_OCEAN_REGION_SFO_1 = new Region("sfo1");
    private static final String DIGITAL_OCEAN_SIZE_512MB = "512mb";

    private DigitalOceanHostConfig() {
    }

    public static HostConfiguration createConfiguration(String hostName) {
        Droplet droplet = new Droplet();

        droplet.setSize(DIGITAL_OCEAN_SIZE_512MB);
        droplet.setImage(DIGITAL_OCEAN_IMAGE_DEBIAN_8_X64);
        droplet.setRegion(DIGITAL_OCEAN_REGION_SFO_1);
        droplet.setEnableBackup(Boolean.FALSE);
        droplet.setEnableIpv6(Boolean.FALSE);
        droplet.setEnablePrivateNetworking(Boolean.FALSE);
        droplet.setKeys(Collections.singletonList(new Key(Config.getInt(Config.Key.DIGITAL_OCEAN_KEY_ID))));

        return new HostConfiguration(hostName, droplet);
    }

    public static void configureProvisioners(HostConfiguration hostConfiguration, Set<HostConfiguration> provisionedHosts) throws Exception {
        String hostName = hostConfiguration.getHostName();

        switch(hostName) {
            case DIGITAL_OCEAN_HOST_CAM_DEV:
                String linodeFremontHubHostName = LinodeHostConfig.LINODE_HOST_FREMONT_HUB;
                String linodeFremontHubIpv4 = null;

                for(HostConfiguration provisionedHost : provisionedHosts) {
                    if(linodeFremontHubHostName.equals(provisionedHost.getHostName())) {
                        linodeFremontHubIpv4 = provisionedHost.getIpv4Address();
                        break;
                    }
                }

                if(StringUtils.isBlank(linodeFremontHubIpv4)) {
                    throw new Exception("Could not find host: " + linodeFremontHubHostName + " needed to provision: " + hostName);
                }

                String hostHubDir = Config.getString(Config.Key.HOST_HUB_DIR);
                String hostFileDir = Config.getString(Config.Key.HOST_FILE_DIR);
                String hostTmpDir = Config.getString(Config.Key.HOST_TMP_DIR);
                String hostMysqlDataDir = Config.getString(Config.Key.MYSQL_DATA_DIR);
                ProvisionFile camUploadsTgz = ProvisionFile.CAM_UPLOADS_TGZ;
                ProvisionFile camSqlGz = ProvisionFile.CAM_SQL_GZ;
                String camSrcDir = Config.getString(Config.Key.CAM_SRC_DIR);
                String camSqlGzHostPath = hostFileDir + "/" + camSqlGz.getFileName();
                String crosstalkUser = Config.getString(Config.Key.CROSSTALK_USER);
                String crosstalkPubKeyHostPath = hostTmpDir + "/crosstalk_pub";
                String crosstalkPvtKeyHostPath = hostTmpDir + "/crosstalk_pvt";
                String sshUser = Config.getString(Config.Key.SSH_USER);

                hostConfiguration
                    .withProvisionImage(ProvisionImage.WORDPRESS)
                    .withProvisionFile(camSqlGz, camSqlGzHostPath)
                    .withProvisionFile(camUploadsTgz, hostFileDir + "/" + camUploadsTgz.getFileName())
                    .withDirectory(hostTmpDir, sshUser, "0700")
                    .withDirectory(hostMysqlDataDir, "root", "0777")
                    .withProvisionFilesAndScript(
                        ProvisionScript.CROSSTALK, ImmutableSet.of(
                            Pair.of(Config.getFile(Config.Key.CROSSTALK_PUB_KEY), crosstalkPubKeyHostPath)
                        ),
                        ImmutableMap.of(
                            "crosstalk_user", crosstalkUser,
                            "crosstalk_key_path", crosstalkPubKeyHostPath
                        )
                    )
                    .withProvisionScript(
                        ProvisionScript.MYSQL,
                        ImmutableMap.of(
                            "data_dir", hostMysqlDataDir,
                            "root_password", Config.getString(Config.Key.MYSQL_ROOT_PASSWORD)
                        )
                    ).withProvisionFilesAndScript(
                        ProvisionScript.CAMDOTCOM, ImmutableSet.of(
                            Pair.of(Config.getFile(Config.Key.CROSSTALK_PVT_KEY), crosstalkPvtKeyHostPath)
                        ),
                        ImmutableMap.builder()
                            .put("cam_src_dir", camSrcDir)
                            .put("crosstalk_host", linodeFremontHubIpv4)
                            .put("crosstalk_key_path", crosstalkPvtKeyHostPath)
                            .put(
                                "crosstalk_passphrase",
                                Config.getString(Config.Key.CROSSTALK_PASSPHRASE)
                                    .replaceAll("\\$", "\\\\x24").replaceAll("\\\"", "\\\\x22").replaceAll("\\\'", "\\\\x27")
                            )
                            .put("crosstalk_user", crosstalkUser)
                            .put("rsync_crosstalk_dir", hostHubDir + "/camdotcom/master")
                            .put("rsync_timeout", Long.toString(Config.getLong(Config.Key.CAM_RSYNC_TIMEOUT)))
                            .put("ssh_port", Integer.toString(Config.getInt(Config.Key.SSH_PORT)))
                            .put("ssh_user", sshUser)
                            .build()
                    ).withProvisionScript(
                        ProvisionScript.WORDPRESS,
                        ImmutableMap.builder()
                            .put("cam_sql_gz", camSqlGzHostPath)
                            .put("container_desc", "dev.conflictareamanagement.com WordPress")
                            .put("container_name", "cam_wp")
                            .put("docker_image_name", ProvisionImage.WORDPRESS.getImageName())
                            .put("docker_image_tag", ProvisionImage.WORDPRESS.getImageTag())
                            .put("mysql_root_password", Config.getString(Config.Key.MYSQL_ROOT_PASSWORD))
                            .put("src_dir", camSrcDir)
                            .put("wordpress_db_name", Config.getString(Config.Key.CAM_DB_NAME))
                            .put("wordpress_db_user", Config.getString(Config.Key.CAM_DB_USER))
                            .put("wordpress_db_password", Config.getString(Config.Key.CAM_DB_PASSWORD))
                            .build()
                    );
                break;
        }
    }
}
