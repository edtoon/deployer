package com.exesus.deployer.config;

import com.myjeeva.digitalocean.pojo.Droplet;
import org.apache.commons.lang3.tuple.Pair;

import java.io.File;
import java.util.*;

public class HostConfiguration {
    private Droplet droplet = null;
    private LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration = null;
    private String ipv4Address = null;

    private Map<String, Pair<String, String>> directoryAndPermissionsMap = new HashMap<>();
    private Set<Pair<ProvisionFile, String>> provisionFilesAtPaths = new HashSet<>();
    private Set<ProvisionImage> provisionImages = new LinkedHashSet<>();
    private Map<ProvisionScript, Object> provisionScriptTemplateDataMap = new LinkedHashMap<>();
    private Map<ProvisionScript, Set<Pair<File, String>>> provisionScriptFileMap = new LinkedHashMap<>();

    public HostConfiguration(String hostName, Droplet droplet) {
        this.droplet = droplet;
        this.linodeHostConfiguration = null;

        setHostName(hostName);
    }

    public HostConfiguration(String hostName, LinodeHostConfig.LinodeHostConfiguration linodeHostConfiguration) {
        this.droplet = null;
        this.linodeHostConfiguration = linodeHostConfiguration;

        setHostName(hostName);
    }

    public String getHostName() {
        if(droplet != null) {
            return droplet.getName();
        } else if(linodeHostConfiguration != null) {
            return linodeHostConfiguration.getLabel().replace('_', '.');
        }

        return null;
    }

    public void setHostName(String hostName) {
        if(droplet != null) {
            droplet.setName(hostName);
        } else if(linodeHostConfiguration != null) {
            linodeHostConfiguration.setLabel(hostName.replace('.', '_'));
        } else {
            throw new RuntimeException("Could not set hostname: " + hostName + " on a configuration with no underlying instance");
        }
    }

    public Droplet getDroplet() {
        return droplet;
    }

    public void setDroplet(Droplet droplet) {
        this.droplet = droplet;
    }

    public LinodeHostConfig.LinodeHostConfiguration getLinodeHostConfiguration() {
        return linodeHostConfiguration;
    }

    public String getIpv4Address() {
        return ipv4Address;
    }

    public void setIpv4Address(String ipv4Address) {
        this.ipv4Address = ipv4Address;
    }

    public HostConfiguration withDirectory(String remotePath, String owner, String mode) {
        directoryAndPermissionsMap.put(remotePath, Pair.of(owner, mode));
        return this;
    }

    public Set<String> getDirectories() {
        return directoryAndPermissionsMap.keySet();
    }

    public Pair<String, String> getDirectoryOwnerAndMode(String remotePath) {
        return directoryAndPermissionsMap.get(remotePath);
    }

    public HostConfiguration withProvisionFile(ProvisionFile provisionFile, String hostPath) {
        provisionFilesAtPaths.add(Pair.of(provisionFile, hostPath));
        return this;
    }

    public Set<Pair<ProvisionFile, String>> getProvisionFilesAtPaths() {
        return provisionFilesAtPaths;
    }

    public HostConfiguration withProvisionImage(ProvisionImage provisionImage) {
        provisionImages.add(provisionImage);
        return this;
    }

    public Set<ProvisionImage> getProvisionImages() {
        return provisionImages;
    }


    public HostConfiguration withProvisionFilesAndScript(ProvisionScript provisionScript, Set<Pair<File, String>> provisionFilesAtPaths) {
        return withProvisionFilesAndScript(provisionScript, provisionFilesAtPaths, null);
    }

    public HostConfiguration withProvisionFilesAndScript(ProvisionScript provisionScript, Set<Pair<File, String>> provisionFilesAtPaths, Object templateData) {
        provisionScriptFileMap.put(provisionScript, provisionFilesAtPaths);
        return withProvisionScript(provisionScript, templateData);
    }

    public Set<Pair<File, String>> getProvisionFilesForScript(ProvisionScript provisionScript) {
        return provisionScriptFileMap.get(provisionScript);
    }

    public HostConfiguration withProvisionScript(ProvisionScript provisionScript) {
        return withProvisionScript(provisionScript, null);
    }

    public HostConfiguration withProvisionScript(ProvisionScript provisionScript, Object templateData) {
        provisionScriptTemplateDataMap.put(provisionScript, templateData);
        return this;
    }

    public Set<ProvisionScript> getProvisionScripts() {
        return provisionScriptTemplateDataMap.keySet();
    }

    public Object getProvisionScriptTemplateData(ProvisionScript provisionScript) {
        return provisionScriptTemplateDataMap.get(provisionScript);
    }
}
