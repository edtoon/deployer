package com.exesus.deployer.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class LinodeHostConfig {
    public static final String LINODE_DISK_TYPE_DEFAULT = "ext4";
    public static final String LINODE_DISK_TYPE_SWAP = "swap";
    public static final String LINODE_HOST_FREMONT_HUB = "hub.linode-fremont.exesus.net";

    private static final String LINODE_CONFIG_DEBIAN_STD = "ExesusDebian8";
    private static final String LINODE_DATACENTER_FREMONT = "fremont";
    private static final String LINODE_DISTRIBUTION_DEBIAN_8 = "Debian 8.1";
    private static final String LINODE_GROUP_EXESUS = "Exesus";

    private LinodeHostConfig() {
    }

    public static HostConfiguration createConfiguration(String hostName) throws Exception {
        LinodeHostDisk primaryDisk = new LinodeHostDisk();

        primaryDisk.setLabel("Primary");
        primaryDisk.setIsReadOnly(false);
        primaryDisk.setIsFromDistribution(true);

        LinodeHostDisk swapDisk = new LinodeHostDisk();

        swapDisk.setLabel("Swap");
        swapDisk.setType(LinodeHostConfig.LINODE_DISK_TYPE_SWAP);
        swapDisk.setIsReadOnly(false);
        swapDisk.setSize(256);

        LinodeHostConfiguration linodeHostConfiguration = new LinodeHostConfiguration();

        linodeHostConfiguration.setDisplayGroup(LINODE_GROUP_EXESUS);
        linodeHostConfiguration.setDataCenterAbbreviation(LINODE_DATACENTER_FREMONT);
        linodeHostConfiguration.setPlanRequiredRam(1024);
        linodeHostConfiguration.setOsDistribution(LINODE_DISTRIBUTION_DEBIAN_8);
        linodeHostConfiguration.setConfigLabel(LINODE_CONFIG_DEBIAN_STD);
        linodeHostConfiguration.addDisk(primaryDisk);
        linodeHostConfiguration.addDisk(swapDisk);

        return new HostConfiguration(hostName, linodeHostConfiguration);
    }

    public static void configureProvisioners(HostConfiguration hostConfiguration, Set<HostConfiguration> provisionedHosts) throws Exception {
        switch(hostConfiguration.getHostName()) {
            case LINODE_HOST_FREMONT_HUB:
                String hostHubDir = Config.getString(Config.Key.HOST_HUB_DIR);
                String hostTmpDir = Config.getString(Config.Key.HOST_TMP_DIR);
                String bitbucketKeyHostPath = hostTmpDir + "/bitbucket_key";
                String crosstalkUser = Config.getString(Config.Key.CROSSTALK_USER);
                String crosstalkKeyHostPath = hostTmpDir + "/crosstalk_key";
                String sshUser = Config.getString(Config.Key.SSH_USER);

                hostConfiguration
                    .withDirectory(hostTmpDir, sshUser, "0700")
                    .withDirectory(hostHubDir, crosstalkUser, "0700")
                    .withProvisionFilesAndScript(
                        ProvisionScript.BITBUCKET,
                        ImmutableSet.of(
                            Pair.of(Config.getFile(Config.Key.BITBUCKET_PVT_KEY), bitbucketKeyHostPath)
                        ),
                        ImmutableMap.of(
                            "ssh_user", sshUser,
                            "key_path", bitbucketKeyHostPath
                        )
                    )
                    .withProvisionFilesAndScript(
                        ProvisionScript.CROSSTALK,
                        ImmutableSet.of(
                            Pair.of(Config.getFile(Config.Key.CROSSTALK_PUB_KEY), crosstalkKeyHostPath)
                        ),
                        ImmutableMap.of(
                            "crosstalk_user", Config.getString(Config.Key.CROSSTALK_USER),
                            "key_path", crosstalkKeyHostPath
                        )
                    )
                    .withProvisionScript(
                        ProvisionScript.BITBUCKET_HUB,
                        ImmutableMap.builder()
                            .put("hub_checkout_dir", hostHubDir + "/camdotcom/master")
                            .put("crosstalk_user", crosstalkUser)
                            .put(
                                "private_key_passphrase",
                                Config.getString(Config.Key.BITBUCKET_PASSPHRASE)
                                    .replaceAll("\\$", "\\\\x24").replaceAll("\\\"", "\\\\x22").replaceAll("\\\'", "\\\\x27")
                            )
                            .put("git_args", "--depth 1")
                            .put("git_repository", "git@bitbucket.org:conflictarea/camdotcom.git")
                            .put("git_timeout", Long.toString(Config.getLong(Config.Key.CAM_GIT_TIMEOUT)))
                            .build()
                    );
                break;
        }
    }

    public enum LinodeHostStatus {
        BEING_CREATED(-1), BRAND_NEW(0), RUNNING(1), POWERED_OFF(2);

        LinodeHostStatus(int status) {
            this.status = status;
        }

        private int status;

        public int getStatus() {
            return status;
        }

        public static LinodeHostStatus valueOfQuietly(int status) {
            for(LinodeHostStatus hostStatus : values()) {
                if(hostStatus.getStatus() == status) {
                    return hostStatus;
                }
            }

            return null;
        }
    }

    public enum LinodeDiskStatus {
        READY(1), BEING_DELETED(2);

        LinodeDiskStatus(int status) {
            this.status = status;
        }

        private int status;

        public int getStatus() {
            return status;
        }

        public static LinodeDiskStatus valueOfQuietly(int status) {
            for(LinodeDiskStatus diskStatus : values()) {
                if(diskStatus.getStatus() == status) {
                    return diskStatus;
                }
            }

            return null;
        }
    }

    public static class LinodeHostConfiguration {
        private String dataCenterAbbreviation;
        private Integer dataCenterId;
        private int planRequiredRam;
        private Integer planId;
        private String label;
        private String displayGroup;
        private Integer linodeId;
        private String osDistribution;
        private Integer distributionId;
        private Integer kernelId;
        private Integer totalHd;
        private Integer status;
        private String configLabel;
        private Integer configId;
        private Set<LinodeHostDisk> disks = new LinkedHashSet<LinodeHostDisk>();

        public Integer getDataCenterId() {
            return dataCenterId;
        }

        public void setDataCenterId(Integer dataCenterId) {
            this.dataCenterId = dataCenterId;
        }

        public String getDataCenterAbbreviation() {
            return dataCenterAbbreviation;
        }

        public void setDataCenterAbbreviation(String dataCenterAbbreviation) {
            this.dataCenterAbbreviation = dataCenterAbbreviation;
        }

        public int getPlanRequiredRam() {
            return planRequiredRam;
        }

        public void setPlanRequiredRam(int planRequiredRam) {
            this.planRequiredRam = planRequiredRam;
        }

        public Integer getPlanId() {
            return planId;
        }

        public void setPlanId(Integer planId) {
            this.planId = planId;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getDisplayGroup() {
            return displayGroup;
        }

        public void setDisplayGroup(String displayGroup) {
            this.displayGroup = displayGroup;
        }

        public Integer getLinodeId() {
            return linodeId;
        }

        public void setLinodeId(Integer linodeId) {
            this.linodeId = linodeId;
        }

        public String getOsDistribution() {
            return osDistribution;
        }

        public void setOsDistribution(String osDistribution) {
            this.osDistribution = osDistribution;
        }

        public Integer getDistributionId() {
            return distributionId;
        }

        public void setDistributionId(Integer distributionId) {
            this.distributionId = distributionId;
        }

        public Integer getKernelId() {
            return kernelId;
        }

        public void setKernelId(Integer kernelId) {
            this.kernelId = kernelId;
        }

        public Integer getTotalHd() {
            return totalHd;
        }

        public void setTotalHd(Integer totalHd) {
            this.totalHd = totalHd;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public String getConfigLabel() {
            return configLabel;
        }

        public void setConfigLabel(String configLabel) {
            this.configLabel = configLabel;
        }

        public Integer getConfigId() {
            return configId;
        }

        public void setConfigId(Integer configId) {
            this.configId = configId;
        }

        public Set<LinodeHostDisk> getDisks() {
            return disks;
        }

        public void addDisk(LinodeHostDisk disk) {
            disks.add(disk);
        }

        public void removeDisk(LinodeHostDisk removeDisk) {
            for(Iterator<LinodeHostDisk> diskIterator = disks.iterator(); diskIterator.hasNext(); ) {
                LinodeHostDisk disk = diskIterator.next();

                if(disk.getDiskId() != null && disk.getDiskId().equals(removeDisk.getDiskId())) {
                    diskIterator.remove();
                } else if(!StringUtils.isBlank(disk.getLabel()) && disk.getLabel().equals(removeDisk.getLabel())) {
                    diskIterator.remove();
                }
            }
        }
    }

    public static class LinodeHostDisk {
        private String label;
        private Integer diskId;
        private String type;
        private Boolean isReadOnly;
        private Integer status;
        private Integer size;
        private Boolean isFromDistribution;

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public Integer getDiskId() {
            return diskId;
        }

        public void setDiskId(Integer diskId) {
            this.diskId = diskId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Boolean getIsReadOnly() {
            return isReadOnly;
        }

        public void setIsReadOnly(Boolean isReadOnly) {
            this.isReadOnly = isReadOnly;
        }

        public Integer getStatus() {
            return status;
        }

        public void setStatus(Integer status) {
            this.status = status;
        }

        public Integer getSize() {
            return size;
        }

        public void setSize(Integer size) {
            this.size = size;
        }

        public Boolean getIsFromDistribution() {
            return isFromDistribution;
        }

        public void setIsFromDistribution(Boolean isFromDistribution) {
            this.isFromDistribution = isFromDistribution;
        }
    }
}
