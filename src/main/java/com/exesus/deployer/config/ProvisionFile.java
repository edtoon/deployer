package com.exesus.deployer.config;

import java.io.File;
import java.util.concurrent.TimeUnit;

public enum ProvisionFile {
    CAM_SQL_GZ("cam.sql.gz", Config.getFile(Config.Key.CAM_SQL_GZ), TimeUnit.MINUTES.toMillis(3)),
    CAM_UPLOADS_TGZ("uploads.tar.gz", Config.getFile(Config.Key.CAM_UPLOADS_TGZ), TimeUnit.MINUTES.toMillis(3));

    private final String fileName;
    private final File localFile;
    private final long executionTimeoutInMilliseconds;

    ProvisionFile(String fileName, File localFile, long executionTimeoutInMilliseconds) {
        this.fileName = fileName;
        this.localFile = localFile;
        this.executionTimeoutInMilliseconds = executionTimeoutInMilliseconds;
    }

    public String getFileName() {
        return fileName;
    }

    public File getLocalFile() {
        return localFile;
    }

    public long getExecutionTimeoutInMilliseconds() {
        return executionTimeoutInMilliseconds;
    }
}
