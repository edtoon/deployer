package com.exesus.deployer.config;

import java.util.concurrent.TimeUnit;

public enum ProvisionImage {
    WORDPRESS("exesus/wordpress", "latest", "images/wordpress", TimeUnit.MINUTES.toMillis(3));

    private String imageName;
    private String imageTag;
    private String imagePath;
    private long executionTimeoutInMilliseconds;

    ProvisionImage(String imageName, String imageTag, String imagePath, long executionTimeoutInMilliseconds) {
        this.imageName = imageName;
        this.imageTag = imageTag;
        this.imagePath = imagePath;
        this.executionTimeoutInMilliseconds = executionTimeoutInMilliseconds;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageTag() {
        return imageTag;
    }

    public String getImagePath() {
        return imagePath;
    }

    public long getExecutionTimeoutInMilliseconds() {
        return executionTimeoutInMilliseconds;
    }
}
