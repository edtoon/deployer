package com.exesus.deployer.config;

import java.io.File;
import java.util.concurrent.TimeUnit;

public enum ProvisionScript {
    // TODO fix hystrix group so individual timeouts are actually used instead of just the first one
    BITBUCKET("scripts/configure-bitbucket.ftl", TimeUnit.MINUTES.toMillis(3)),
    BITBUCKET_HUB("scripts/configure-hub-bitbucket.ftl", TimeUnit.MINUTES.toMillis(3)),
    CAMDOTCOM("scripts/configure-camdotcom.ftl", TimeUnit.MINUTES.toMillis(3)),
    CLOCK("scripts/configure-clock.sh", TimeUnit.MINUTES.toMillis(3)),
    CROSSTALK("scripts/configure-crosstalk.ftl", TimeUnit.MINUTES.toMillis(3)),
    DOCKER("scripts/configure-docker.sh", TimeUnit.MINUTES.toMillis(3)),
    MYSQL("scripts/configure-mysql.ftl", TimeUnit.MINUTES.toMillis(3)),
    SSH("scripts/configure-ssh.ftl", TimeUnit.MINUTES.toMillis(10)),
    UPDATES("scripts/configure-updates.sh", TimeUnit.MINUTES.toMillis(5)),
    WORDPRESS("scripts/configure-wordpress.ftl", TimeUnit.MINUTES.toMillis(3));

    private File scriptFile;
    private long executionTimeoutInMilliseconds;

    ProvisionScript(String scriptPath, long executionTimeoutInMilliseconds) {
        this.scriptFile = new File(Config.EXESUS_DIR, scriptPath);
        this.executionTimeoutInMilliseconds = executionTimeoutInMilliseconds;
    }

    public File getScriptFile() {
        return scriptFile;
    }

    public long getExecutionTimeoutInMilliseconds() {
        return executionTimeoutInMilliseconds;
    }
}
